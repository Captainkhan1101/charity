<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    //
    protected $table = 'categories';
    protected $fillable = ['name', 'description', 'photo', 'parent_id'];
    protected $dates = ['deleted_at'];

    public function categorySeo(){
        return $this->hasOne(CategorySeo::class);
    }
}
