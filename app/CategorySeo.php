<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategorySeo extends Model
{
    //
    protected $table = 'category_seo';
    protected $fillable = ['seo_title', 'seo_description', 'seo_url', 'category_id'];
    protected $dates = ['deleted_at'];
}
