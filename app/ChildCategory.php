<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ChildCategory extends Model
{
    //
    protected $table = 'childcategories';
    protected $fillable = ['child_name', 'subcategory_id'];
    protected $dates = ['deleted_at'];
}
