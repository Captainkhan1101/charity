<?php

namespace App\Helper;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;
use Config;

class BreadCrumHelper
{
    public static  function getBreadCrum($index = ''){
        $response = [];
        $response = Config::get('breadcrum.'.$index);
        return $response;
    }


}
