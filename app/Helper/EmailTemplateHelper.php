<?php

namespace App\Helper;


use App\EmailTemplate;
use App\Http\Controllers\EmailController;
use App\User;
use Intervention\Image\ImageManagerStatic as Image;

class EmailTemplateHelper
{
    public static function get($data,$name)
    {
        $data['{$baseURL}'] = url('/');
        $content = EmailTemplate::search('name',$name)->pluck('format')->first();
        return strtr( $content, $data);
    }

    public static function sendVerificationEmail(User $user){
        $name = 'verify-account';
        $data = [
            '{$email}'=>encrypt($user->getEmail()),
            '{$name}'=>$user->getName(),
        ];
        $content = self::get($data,$name);
        (new EmailController())->sendEmail([
                'content' => $content
            ],
            $user->getEmail(),
            'dashboard.emails.body',
            __('info.subjects.verify')
        );
    }
}
