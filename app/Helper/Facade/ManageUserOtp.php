<?php

namespace App\Helper\Facade;

use Illuminate\Support\Facades\Facade;

class ManageUserOtp extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'user_otp';
    }
}
