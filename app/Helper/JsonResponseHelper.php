<?php

namespace App\Helper;


class JsonResponseHelper
{
    public static function defaultCreate()
    {
        return response()->json(
            [
                'message'=>__('messages.default_created'),
                'state'=>'success'
            ], 200);
    }
    public static function defaultUpdate()
    {
        return response()->json(
            [
                'message'=>__('messages.default_updated'),
                'state'=>'success'
            ], 200);
    }
    public static function defaultDelete()
    {
        return response()->json(
            [
                'message'=>__('messages.default_deleted'),
                'state'=>'success'
            ], 200);
    }
    public static function alreadyLogin()
    {
        return response()->json(
            [
                'message'=>__('messages.login_already'),
                'state'=>'success'
            ], 200);
    }
    public static function unauthenticUser()
    {
        return response()->json(
            [
                'message'=>__('messages.unauthorized'),
                'state'=>'error'
            ],401);
    }
    public static function logoutUser()
    {
        return response()->json(
            [
                'message'=>__('messages.logout'),
                'state'=>'success',
                'url'=> Config::get('app.url')
            ],
            200);
    }
}
