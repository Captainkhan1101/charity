<?php

namespace App\Helper;

use App\Models\Upload;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;
use DB;
use Illuminate\Support\Facades\Input;

class SecurityHelper
{
    public static function isDecryptValid($value){
        try{
            $response = decrypt($value);
        }catch(\Exception $e){
            $response = false;
        }
        return $response;
    }
}
