<?php

namespace App\Helper;

use Illuminate\Support\Facades\File;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Intervention\Image\ImageManagerStatic as Image;

class UploadHelper
{
    /**
     * Get the registered name of the component.
     *
     * @param $image
     * @param bool $is_base_64
     * @return string
     */
    public static function uploadAvatar($image,$is_base_64 = false)
    {
        return self::uploadFile($image,'image',null,true,$is_base_64);
    }
    public static function resizeImage($image,$width = 500,$height = 500){
        return Image::make($image)->resize($width,$height,function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        });
    }
    public static function uploadFile($file,$type = 'image', $name = null,$resize = true,$is_base_64 = false,$destination = null,$width = 500,$height = 500){
        if(empty($destination)){
            $destination =  config('restaurant.upload_directory');
        }
        if($type === 'image'){

            if(empty($name)){
                $name =  date("Y-m-d-His-").Str::random(10).'.png';
            }
            if(!$is_base_64){
                $file = $file->getRealPath();
            }
            if($resize){
                $file = self::resizeImage($file,$width,$height);
            }else{
                $file = Image::make($file);
            }
        }
        $file->save(storage_path('app/public/'.$destination) . $name);
        return 'storage/'.$destination . $name;
    }
    public static function getImage($path,$default = 'assets/images/portrait/small/avatar-s-19.png'){
        return !empty($path) ? asset('images/'.$path) : asset($default);
    }
    public static function getPhoto($pic,$default = 'assets/images/portrait/small/avatar-s-19.png'){
        $path = asset($pic);
        return !empty($pic) ? $path : asset($default);
    }
}
