<?php

namespace App\Http\Controllers\Auth;

use App\Helper\Facade\ManageUserOtp;
use App\Http\Controllers\Controller;
use App\Http\Controllers\MailController;
use App\Http\Requests\ForgetPasswordRequest;
use App\User;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Str;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;
    public function send(Request $request){
        $response = null;
        DB::transaction(function() use(&$response,$request){
            $email = $request->input('email');
            $number = $request->input('number');
            $code = Str::random(6);
            ManageUserOtp::store($email,$number,$code);
            $response =  (new MailController($email))->ForgetPassword($code);
        });
        return $response;
    }
    public function sendForgetPassword(Request $request){
        $credentials = request()->validate(['email' => 'required|email']);
        Password::sendResetLink($credentials);
        return response()->json(["msg" => 'Reset password link sent on your Email'],200);
    }
    public function sendForgetPasswordLink(Request $request){
        $credentials = request()->validate(['email' => 'required|email']);
        Password::sendResetLink($credentials);
        return response()->json(["msg" => 'Reset password link sent on your Email'],200);
    }
    public function verifyCode(ForgetPasswordRequest $request){

        $response = null;
        DB::transaction(function() use(&$response,$request){
            $code = $request->input('code');
            $email = $request->input('email');
            $response =  ManageUserOtp::verifyCode($email,$code);
        });
        return $response;
    }
    public function updatePassword(ForgetPasswordRequest $request){
            
        $response = null;
        DB::transaction(function() use(&$response,$request){
            $code = $request->input('code');
            $email = $request->input('email');
            $password = $request->input('password');
            
            $user = User::where('email', $email)->first();
            $user->password = Hash::make($password);
            $user->save();
        });
        Session::flash('message', 'password updated!'); 
            Session::flash('alert-class', 'alert-success'); 
            return back();
    }
}
