<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Validator;
use Auth;
use Session;
use Redirect;
class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;
    public function resetpwd(Request $request)
    {

        $Validator =  Validator::make($request->all(), [
            'old_password' =>'required',
            'password' => 'required|string|min:6|confirmed',
        ]);
        if($Validator->fails()){
            return Redirect::route('create.reset.password')->with(['errors' => $Validator->errors() ]);
        }

        if(!Hash::check($request->old_password,Auth::user()->password)){

            Session::flash('message', 'The specified password does not match!');
            Session::flash('alert-class', 'alert-danger');
            return back();
        }
        else{
            $request->user()->fill(['password' => Hash::make($request->password)])->save();
            Session::flash('message', 'password updated!');
            Session::flash('alert-class', 'alert-success');
            return back();

        }
    }

}
