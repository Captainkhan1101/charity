<?php

namespace App\Http\Controllers;

use App\Category;
use App\Helper\UploadHelper;

use Illuminate\Http\Request;
use Session;
use Illuminate\Database\QueryException;
use DB;
use App\CategorySeo;

class CategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $categories = Category::all();
        return view('admin.category.listing')->with(['categories' => $categories]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.category.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        //

        $id = $request->id;
        $category = Category::where('id', $id)->with('categorySeo')->first();

        return view('admin.category.edit')->with(['category' => $category]);

    }
    public function subCategoryEdit(Request $request)
    {
        //

        $id = $request->id;
        $categories = Category::all();
        $category = Category::where('id', $id)->with('categorySeo')->first();
//           dd($category);
        return view('admin.category.sub-category.edit')->with(['category' => $category, 'categories' => $categories]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        //
        $id = $request->id;
        $category = Category::where('id', $id)->first();

        if(!empty($category)){
            try {
                $category->delete();
                // Closures include ->first(), ->get(), ->pluck(), etc.
            } catch(\Illuminate\Database\QueryException $ex){

                if($ex->errorInfo['1'] == 1451){
                    session::flash('alert-class',"alert-danger");
                    session::flash('message', "Can not Delete it, because this category used in products");
                    return back();
                }


                // Note any method of class PDOException can be called on $ex.
            }

            session::flash('alert-class',"alert-danger");
            session::flash('message','Category Deleted!');
            return back();

        }else{
            session::flash('alert-class',"alert-danger");
            session::flash('message','Category not found!');
            return back();
        }
    }

    public function save(Request $request){
//              dd($request->all());
        if(!empty($request->id)){
            $dataCategory = [
                'name' => $request->title,
                'photo' => !empty($request->file('photo')) && $request->hasFile('photo')
                ? UploadHelper::uploadAvatar($request->file('photo'))
                : '0',
                'description' => $request->input('description')
            ];
             $category = Category::where('id',$request->id)->first();
             if(!empty($category)){
                 $category->update($dataCategory);
                 session::flash('alert-class',"alert-success");
                 session::flash('message','Category Created Successfully!');
                 return back();
             }
            session::flash('alert-class',"alert-danger");
            session::flash('message','Category not found!');
            return back();




        }else{
            $dataCategory = [
                'name' => $request->title,
                'photo' => !empty($request->file('photo')) && $request->hasFile('photo')
                    ? UploadHelper::uploadAvatar($request->file('photo'))
                    : '0',
                'description' => $request->input('description')
            ];

            Category::insert($dataCategory);
              $lastInsertedId = DB::getPdo()->lastInsertId();
            $dataCategoryData = [];
            $dataCategoryData = [
                'seo_title' =>  $request->title,
                'seo_description' =>  $request->seo_description,
                'seo_url' =>  $request->seo_url,
                'category_id' =>  $lastInsertedId,

            ];

            CategorySeo::insert($dataCategoryData);

            session::flash('alert-class',"alert-success");
            session::flash('message','Category Created Successfully!');
            return back();
        }



    }

    public function createSub(Request $request){

        $id = $request->id;
        $category  = Category::where('id',$id )->first();

        return view('admin.category.sub-category.create')->with(['category' => $category]);
    }

    public function subCategoryStore(Request $request){
//              dd($request->all());
        if(!empty($request->id)){
            $dataCategory = [];
            $dataCategory = [
                'name' => $request->title,
                'photo' => !empty($request->file('photo')) && $request->hasFile('photo')
                    ? UploadHelper::uploadAvatar($request->file('photo'))
                    : '0',
                'description' => $request->input('description'),
                'parent_id' => $request->input('category_id')
            ];

            $category = Category::where('id',$request->id)->first();

            if(!empty($category)){
                $category->update($dataCategory);

                $categorySeo = CategorySeo::where('category_id', $request->id )->first();

                if(!empty($categorySeo)){
                    $categorySeoData =[
                    'seo_title' =>   $request->input('seo_title'),
                    'seo_description' =>   $request->input('seo_description'),
                    'seo_url' =>   $request->input('seo_url'),
                    'category_id' =>   $request->input('id'),
                        ];

                    $categorySeo->update($categorySeoData);
                }

                session::flash('alert-class',"alert-success");
                session::flash('message','Category Updated Successfully!');
                return back();
            }
            session::flash('alert-class',"alert-danger");
            session::flash('message','Category not found!');
            return back();




        }else{
            $dataCategory = [
                'name' => $request->title,
                'photo' => !empty($request->file('photo')) && $request->hasFile('photo')
                    ? UploadHelper::uploadAvatar($request->file('photo'))
                    : '0',
                'description' => $request->input('description'),
                'parent_id'   => $request->input('category_id')
            ];

            Category::insert($dataCategory);
            $lastInsertedId = DB::getPdo()->lastInsertId();
            $dataCategoryData = [];
            $dataCategoryData = [
                'seo_title' =>  $request->title,
                'seo_description' =>  $request->seo_description,
                'seo_url' =>  $request->seo_url,
                'category_id' =>  $lastInsertedId,

            ];

            CategorySeo::insert($dataCategoryData);

            session::flash('alert-class',"alert-success");
            session::flash('message','Category Created Successfully!');
            return back();
        }
    }

        public function viewSubCategory(Request $request){
                 $id = $request->id;
            $categories  = Category::where('parent_id',$id )->get();

            return view('admin.category.sub-category.listing')->with(['categories' => $categories]);
        }


}
