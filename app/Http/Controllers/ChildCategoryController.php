<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ChildCategory;
use App\SubCategory;
use Session;

class ChildCategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
           $childCategories = ChildCategory::all();
           return view('admin.category.child-category.listing')->with(['childCategories' => $childCategories]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
          $subCategories = SubCategory::all();
        return view('admin.category.child-category.create')->with(['subCategories' => $subCategories]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        //
        $id =  $request->id;
        $subCategories = SubCategory::all();
        $childCategory = ChildCategory::where('id', $id)->first();
        return view('admin.category.child-category.edit')->with(['childCategory' => $childCategory, 'subCategories' => $subCategories]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        //
        //
        $id = $request->id;
        $childCategory = ChildCategory::where('id', $id)->first();

        if(!empty($childCategory)){
            try {
                $childCategory->delete();
                // Closures include ->first(), ->get(), ->pluck(), etc.
            } catch(\Illuminate\Database\QueryException $ex){

                if($ex->errorInfo['1'] == 1451){
                    session::flash('alert-class',"alert-danger");
                    session::flash('message', "Can not Delete it, because this category used in products");
                    return back();
                }


                // Note any method of class PDOException can be called on $ex.
            }

            session::flash('alert-class',"alert-danger");
            session::flash('message','Child Category Deleted!');
            return back();

        }else{
            session::flash('alert-class',"alert-danger");
            session::flash('message','Child Category not found!');
            return back();
        }
    }

    public function save(Request $request)
    {
        //
//        dd($request->all());
        $dataChildCategory = [];

        if(!empty($request->id)){
            $dataChildCategory = [
                'child_name' => $request->name,
                'subcategory_id' => $request->subcategory_id
            ];

             $childCategory= ChildCategory::where('id', $request->id)->first();

             $childCategory->update($dataChildCategory);

            session::flash('alert-class',"alert-success");
            session::flash('message','Child Category Updated Successfully!');
            return back();
        }else{
            $dataChildCategory = [
                'child_name' => $request->name,
                'subcategory_id' => $request->subcategory_id,
            ];
            ChildCategory::insert($dataChildCategory);

            session::flash('alert-class',"alert-success");
            session::flash('message','Child Category Created Successfully!');
            return back();
        }


    }
}
