<?php

namespace App\Http\Controllers;

use App\Helper\BreadCrumHelper;
use App\Http\Requests\CreatePermissionRequest;
use App\Http\Requests\UpdatePermissionRequest;
use App\Models\RolePermissions;
use App\Http\Resources\Collections\PermissionCollection;
use App\Models\Permission;
use App\Repositories\PermissionRepository;
use App\Models\Role;
use App\Models\User;
use Cassandra\Exception\ValidationException;
use http\Client\Response;
use Illuminate\Contracts\Console\Application;
use Illuminate\Contracts\Translation\Translator;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Session;
use Illuminate\Support\Str;

class PermissionController extends Controller
{

    private $repo = null;

    public function __construct()
    {
        $this->middleware('auth');

    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return PermissionCollection
     */
    public function index(Request $request)
    {


        $permissions = DB::table('permissions');
        $permissions->leftjoin("permission_role","permission_role.permission_id","=","permissions.id");
        $permissions->leftjoin("roles","roles.id","=","permission_role.role_id");
        $permissions->select('permissions.*',DB::raw('group_concat(roles.display_name) as PermissionRoles'));


        $permissions->orderBy("permissions.id" , "DESC");

        $permissions->groupby("permissions.id");

        $permissions = $permissions->get();


        $Roles = Role::Orderby("name","ASC")
            ->get();


        return view('admin.permission.index')->with(['permissions' => $permissions, 'Roles' =>   $Roles]);
    }
    /**
     * Create a new user instance after a valid registration.
     *
     * @param CreatePermissionRequest $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function create()
    {
        $roles  = Role::Orderby("name","ASC")->get();
//        $breadCrum =  BreadCrumHelper::getBreadCrum('create-permission');
        return view('admin.permission.add')->with(['roles'=> $roles]);
    }

    public function store(Request $request)
    {
        $request->validate([
            "name"=>"required",
            "display_name" =>"required ",
            "description"=>"required",
            "roles"=>"required",

        ]);
        $input          = $request->all();
        $input['roles'] = implode(" ,",$request->roles);
        $response       = array();
        if(!empty($input['permission_id']))
        {
            $Permissions    = Permission::where('id',"=",Str::slug($input['permission_id']))->first();
            if(!empty($Permissions) && $Permissions->id != $input['permission_id'])
            {
                $response['status'] = false;
                session::flash('alert-class',"alert-danger");
                session::flash('message','Permission name duplication');
                return back();
            }
            $Permissions    = Permission::where('id',"=",$input['permission_id'])->first();
            if(!empty($input['name']))
                $Permissions->name          =  Str::slug($input['name']);
                $Permissions->display_name  =  $input['display_name'];
                $Permissions->description   =  (!empty($input['description']) ? $input['description'] : '');
                if($Permissions->save())
                {
                    if(!empty($input['roles']))
                    {
                        DB::table('permission_role')->where("permission_id","=",$input['permission_id'])->delete();
                        if(strpos($input['roles'],",") == true)
                        {
                            $user_roles = explode(",", $input['roles']);
                            foreach($user_roles as $role):
                                $Role   = Role::where("id","=",$role)->first();
                                $Role->attachPermission($Permissions);
                                endforeach;
                        }
                        else{
                                $Role   = Role::where("id","=",$input['roles'])->first();
                                $Role->attachPermission($Permissions);
                        }
                    }
                    session::flash('alert-class',"alert-success");
                    session::flash('message','Permission successfully updated');
                    return back();
                }
                else{
                    session::flash('alert-class',"alert-danger");
                    session::flash('message','Permission could not updated');
                    return back();
                }
        }
        else{
            $Permissions              = Permission::where('name',"=",Str::slug($input['name']))->first();
            if(!empty($Permissions))
            {
                session::flash('alert-class',"alert-danger");
                session::flash('message','Permission name duplication');
                return back();
            }
                $Permissions               =new Permission;
                $Permissions->name         =  Str::slug($input['name']);
                $Permissions->display_name =  $input['display_name'];
                $Permissions->description  =  (!empty($input['description']) ? $input['description'] : '');
                if($Permissions->save()){
                    if(!empty($input['roles'])){
                        if(strpos($input['roles'],",") == true)
                        {
                            $user_roles = explode(",", $input['roles']);
                            foreach($user_roles as $role):
                                $Role = Role::where("id","=",$role)->first();
                                $Role->attachPermission($Permissions);
                                endforeach;
                        }
                        else{
                            $Role = Role::where("id","=",$input['roles'])->first();
                            $Role->attachPermission($Permissions);
                        }
                    }
                    session::flash('alert-class',"alert-success");
                    session::flash('message','Permission successfully created');
                    return back();
                }
                else{
                    session::flash('alert-class',"alert-danger");
                    session::flash('message','Permission could not created');
                    return back();
                }
        }
    }

    public function edit(Request $request)
    {

        $permission_id = $request->id;
        $Permissions   = Permission::where('id',"=",$permission_id)->first();
        $Roles         = Role::Orderby("name","ASC")
            ->get();
        $selectedRoles = DB::table('permission_role')->where('permission_role.permission_id',"=",$permission_id)
            ->select("roles.name","roles.id")
            ->join("roles","permission_role.role_id","=","roles.id")
            ->groupby("roles.id")
            ->get();
        if(!empty($Permissions) ){
//            $breadCrum =  BreadCrumHelper::getBreadCrum('permission-edit');
            return view('admin.permission.edit')->with(['Permissions' => $Permissions, 'roles' => $Roles, 'selectedRoles' => $selectedRoles]);
        }
        else{
            Session::flash('alert-class', 'alert-danger');
            Session::flash('message', 'fail');
            return view('dashboard.permission.edit');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdatePermissionRequest $request
     * @return JsonResponse
     */
    public function update(Request $request,$id)
    {
        $request->validate([
            "name"         =>"required",
            "display_name" =>"required ",
            "description"  =>"required",
            "roles"        =>"required",

        ]);
        //        return $this->repo->updateRecord($request,$request->input('id'));
        $roles               = Permission::find($id);
        $roles->name         = $request->input('name');
        $roles->display_name = $request->input('display_name');
        $roles->description  = $request->input('description');
        $roles ->save();
        $permission_id       = $roles->id;
        $roles               = $request->input('roles');
        foreach ($roles as $value)
        {
            $role_permissions = new RolePermissions();
            $role_permissions->permission_id = $permission_id;
            $role_permissions->role_id = $value;
            $role_permissions->timestamps = false;
            $role_permissions->save();
        }
        return redirect('/permission')->with('update', $roles);

    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return array|Application|Translator|string
     */
    public function delete(Request $request)
    {
        $response = array();
        if(Permission::where('id',"=",$request->id)->delete())
        {
            Session::flash('alert-class', 'alert-danger');
            Session::flash('message', 'Successfully deleted');
            return back();
        }
        else{
            Session::flash('alert-class', 'alert-danger');
            Session::flash('message', 'Could not deleted');
            return back();
        }
    }
}
