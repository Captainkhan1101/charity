<?php

namespace App\Http\Controllers;

use App\Helper\UploadHelper;
use App\Http\Resources\ProductCollection;
use App\Product;
use App\ProductVariant;
use App\ProductImage;
use App\Category;
use App\ChildCategory;
use App\SubCategory;
use App\ProductSeo;
use App\ProductCategory;

use App\User;
use App\Http\Requests\ProductSaveRequest;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\View\View;
use Illuminate\Database\QueryException;
use Session;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|View
     */
    private    $productModel = Null;
    private    $productVariantModel = Null;
    private    $productImageModel = Null;
    public function __construct()
    {
        $this->productModel              = new Product();
        $this->productVariantModel       = new ProductVariant();
        $this->productImageModel         = new  ProductImage();
    }

    public function index()
    {
         $product = Product::with(['produtsVariants', 'produtsImages'])->get();

         return view('admin.product.index',[
            'products' => new ProductCollection($product)
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Application|Factory|View
     */
    public function create()
    {
        $categories      =  Category::all();
        $childCategories =  ChildCategory::all();
        $subCategories   =  SubCategory::all();
//        dd($childCategories);

        return view('admin.product.create')->with(['categories' => $categories, 'childCategories' => $childCategories, 'subCategories' => $subCategories ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(ProductSaveRequest $request)
    {
        //

//        dd($request->all());

        $variant_combination = '';
        $variant = '';
        if(!empty($request->input('variant_add'))){
            $variant_combination = implode('/',$request->input('variant_add'));
        }
        if(!empty($request->input('variant'))){
            $variant  = implode('/', $request->input('variant'));
        }


                   if(!empty($request->id)){

//                       $dataProduct = [];
//                       $dataProduct = [
//
//                           'title'                      => $request->input('title'),
//                           'description'                => $request->input('description'),
//                           'photo'                      => $request->file('photo'),
//                           'retail_price'               => $request->input('retail_price'),
//                           'price'                      => $request->input('price'),
//                           'sku'                        => $request->input( 'sku' ),
//                           'barcode'                    => $request->input('barcode'),
//                           'quantity'                   => $request->input('quantity'),
//                           'variant_combination'        => $variant_combination,
//                           'variant'                    => $variant,
//                           'product_availability_date'  => $request->input( 'product_availability_date' ),
//                           'category_id'                => $request->input('category_id'),
//                           'subcategory_id'             => $request->input('subcategory_id'),
//                           'childcategory_id'           => $request->input('childcategory_id')
//
//
//                       ];

                       $product  =  product::where('id', $request->id)->first();
                       $product->title                      = $request->input('title');
                       $product->description                = $request->input('description');
                       if(!empty($request->photo)){
                           $product->photo                  = !empty($request->file('photo')) && $request->hasFile('photo')
                                                               ? UploadHelper::uploadAvatar($request->file('photo'))
                                                               : '0';
                       }
                       $product->retail_price               = $request->input('retail_price');
                       $product->price                      = $request->input('price');
                       $product->sku                        = $request->input( 'sku' );
                       $product->barcode                    = $request->input('barcode');
                       $product->quantity                   = $request->input('quantity');
                       $product->variant_combination        = $variant_combination;
                       $product->variant                    = $variant;
                       $product->product_availability_date  = $request->input( 'product_availability_date' );
//                       $product->category_id                = $request->input('category_id');
//                       $product->subcategory_id             = $request->input('subcategory_id');
//                       $product->childcategory_id           = $request->input('childcategory_id');
                       $product->title =
                       $product->update();

                       $productSeo = ProductSeo::where('product_id',$request->id )->first();
                       if(empty($productSeo)){
                           $productSeo = new ProductSeo;
                           $productSeo->seo_title = $request->input('seo_title');
                           $productSeo->seo_description =   $request->input('seo_description');
                           $productSeo->seo_url       =  $request->input('seo_url');
                           $productSeo->product_id       =  $request->id;
                           $productSeo->save();
                       }else{
                           $productSeo->seo_title = $request->input('seo_title');
                           $productSeo->seo_description =   $request->input('seo_description');
                           $productSeo->seo_url       =  $request->input('seo_url');
                           $productSeo->update();
                       }


                         ProductVariant::where('product_id', $request->id )->delete();

                       if(!empty($request->variant_info)){
                           $productVariant = [];
                           for($x= 0; $x < count($request->variant_info); $x++){


                               $productVariant[]= [
                                   'product_id'  =>  $request->id,
                                  'variant_info' => $request->variant_info[$x],
                                   'price'  =>  $request->input('v_price')[$x],
                                   'quantity'  =>  $request->input('v_quantity')[$x],
                                   'sku'  =>  $request->input('v_sku')[$x],
                                   'barcode'  =>  $request->input('v_barcode')[$x],
                               ];
                           }
                           $this->productVariantModel::insert($productVariant);
                       }

                       ProductImage::where('product_id', $request->id )->delete();
                       $productImage = [];
                       if(!empty($request->gallery)){
                           for ($x = 0; $x < count($request->gallery); $x++){

                               $productImage[] = [
                                   'product_id' => $request->id,
                                   'gallery' => !empty($request->file('gallery')[$x])
                                       ? UploadHelper::uploadAvatar($request->file('gallery')[$x])
                                       : '0',

                               ];
                           }
                           $this->productImageModel::insert($productImage);
                       }
                       session::flash('alert-class',"alert-success");
                       session::flash('message','Product Updated Successfully!');
                       return back();


                   }else{





                       $this->productModel->setTitle($request->input('title'))
                           ->setDescription($request->input('description'))
                           ->setPhoto(!empty($request->file('photo')) && $request->hasFile('photo')
                               ? UploadHelper::uploadAvatar($request->file('photo'))
                               : '0')
                           ->setRetailPrice($request->input('retail_price'))
                           ->setPrice($request->input('price'))
                           ->setSku($request->input( 'sku' ))
                           ->setBarcode(!empty($request->input('barcode'))? $request->input('barcode'): 10)
                           ->setQuantity($request->input('quantity'))
                           ->setVariant($variant)
                           ->setVariantCombination($variant_combination)
                           ->setVariant($variant)
                           ->setWeight($request->input('weight'))
                           ->setUnit($request->input('unit'))
                           ->setWidth($request->input('width'))
                           ->setheight($request->input('height'))
                           ->setProductAvailabilityDate($request->input( 'product_availability_date' ))
//                           ->setCategoryId($request->input('category_id'))
//                           ->setSubCategoryId($request->input('subcategory_id'))
//                           ->setChildCategoryId($request->input('childcategory_id'))
                           ->save();
//          return 'success';
                       $productLastInsertedID  =  $this->productModel->id;

                        $prductSeoData = [];
                       $prductSeoData = [
                           'seo_title' => $request->seo_title,
                           'seo_description' => $request->seo_description,
                           'seo_url' => $request->seo_url,
                           'product_id' => $productLastInsertedID
                       ];
                       ProductSeo::insert($prductSeoData);

                       $productCategoryData = [];
                       if(!empty($request->category_id)){
                           for($x=0; $x < count($request->category_id); $x++){
                               $productCategoryData = [
                                   'product_id' => $productLastInsertedID,
                                   'category_id' => $request->category_id[$x],

                               ];
                           }
                       }


                       ProductCategory::insert($productCategoryData);


                       if(!empty($request->variant_info)){
                           $productVariant = [];
                           for($x= 0; $x < count($request->variant_info); $x++){


                               $productVariant[]= [
                                   'product_id'  =>  $productLastInsertedID,
                                   'variant_info' => $request->input('variant_info')[$x],
                                   'price'  =>  $request->input('v_price')[$x],
                                   'quantity'  =>  $request->input('v_quantity')[$x],
                                   'sku'  =>  $request->input('v_sku')[$x],
                                   'barcode'  =>  !empty($request->input('v_barcode')[$x])? $request->input('v_barcode')[$x]: 10,
                               ];
                           }
                           $this->productVariantModel::insert($productVariant);
                       }

                       $productImage = [];
                       if(!empty($request->gallery)){
                           for ($x = 0; $x < count($request->gallery); $x++){

                               $productImage[] = [
                                   'product_id' => $productLastInsertedID,
                                   'gallery' => !empty($request->file('gallery')[$x])
                                       ? UploadHelper::uploadAvatar($request->file('gallery')[$x])
                                       : '0',

                               ];
                           }
                           $this->productImageModel::insert($productImage);
                       }



                       session::flash('alert-class',"alert-success");
                       session::flash('message','Product Success Successfully!');
                       return back();

                   }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit(Request $request)
    {
        //


          $product = Product::where('id', $request->id)->with(['produtsVariants', 'produtsImages', 'productCategory'])->first();
          $categories      =  Category::all();
          $childCategories =  ChildCategory::all();
          $subCategories   =  SubCategory::all();

//          dd($product);

//        $variant_combinationComa  =  explode('/', $product->variant_combination);
//        $variantArrComa  =  explode('/', $product->variant);
//        dd($variantArrComa);
        return view('admin.product.edit')->with(['product' => $product, 'categories' => $categories, 'childCategories' => $childCategories, 'subCategories' => $subCategories]);


    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function delete(Request $request)
    {
        //
        $id = $request->id;
        $product = Product::where('id', $id)->first();

        if(!empty($product)){
            try {
                $productVariant = ProductVariant::where('product_id', $id )->first();
                if(!empty($productVariant)){
                    $productVariant->delete();
                }
                $productImage = ProductImage::where('product_id', $id )->first();
                if(!empty($productImage)){
                    $productImage->delete();
                }
                $product->delete();




                // Closures include ->first(), ->get(), ->pluck(), etc.
            } catch(\Illuminate\Database\QueryException $ex){

                if($ex->errorInfo['1'] == 1451){
                    session::flash('alert-class',"alert-danger");
                    session::flash('message', "Can not Delete it");
                    return back();
                }


                // Note any method of class PDOException can be called on $ex.
            }

            session::flash('alert-class',"alert-danger");
            session::flash('message','Product Deleted!');
            return back();

        }else{
            session::flash('alert-class',"alert-danger");
            session::flash('message','Product not found!');
            return back();
        }
    }
}
