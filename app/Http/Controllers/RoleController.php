<?php

namespace App\Http\Controllers;

use App\Http\Resources\Collections\RoleCollection;
use App\Http\Resources\Collections\RoleDropdownCollection;
use App\Models\Permission;
use App\Models\Role;
use Illuminate\Http\Request;
use DB;
use Session;
use App\Helper\BreadCrumHelper;
use Illuminate\Support\Str;



class RoleController extends Controller
{




    public function __construct()
    {
        $this->middleware('auth');

    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return RoleCollection
     */
    public function index(Request $request)
    {

            $roles = Role::all();
//        $breadCrum =  BreadCrumHelper::getBreadCrum('role-listing');
        return view('admin.role.index')->with(['roles' => $roles]);
    }
    public function create()
    {
//        $breadCrum =  BreadCrumHelper::getBreadCrum('role-create');
        $show = Role::all();
        return view('admin.role.add');
    }
    public function store(Request $request)
    {
        $request->validate([
            "name"=>"required",
            "display_name" =>"required ",
            "description"=>"required",
        ]);
        $input    = $request->all();
        $response = array();
        if(!empty($input['role_id']))
        {
            $Role = Role::where('id',"=",$input['role_id'])->first();
            if(!empty($Role) && $Role->id != $input['role_id'])
            {
                Session::flash('message', 'Role name duplication!');
                Session::flash('alert-class', 'alert-danger');
                return back()->with(['response' => $response]);
            }
            $Role = Role::where('id',"=",$input['role_id'])->first();
            if(!empty($input['name']))
                $Role->name            =  Str::slug($input['name']);
                $Role->display_name    =  $input['display_name'];
                $Role->description     =  (!empty($input['description']) ? $input['description'] : '');
                if($Role->save()){
                    $roles             =  DB::table('roles');
                    $roles->orderBy("roles.id" , "DESC");
                    $roles             = $roles->get();
                    $response['roles'] = $roles;
                    Session::flash('message', 'Role successfully updated!');
                    Session::flash('alert-class', 'alert-success');
                    return back()->with(['response' => $response]);
                }
                else{
                        Session::flash('message', 'Role could not updated!');
                        Session::flash('alert-class', 'alert-danger');
                        return back()->with(['response' => $response]);
                }
        }
        else{
            $Role = Role::where('name',"=",$input['name'])->first();
                if(!empty($Role) && $Role->count() >0)
                {
                    Session::flash('message', 'Role name duplication!');
                    Session::flash('alert-class', 'alert-danger');
                    return back();
                }
                $Role  =new Role;
                $Role->name         =  Str::slug($input['name']);
                $Role->display_name =  $input['display_name'];
                $Role->description  =  (!empty($input['description']) ? $input['description'] : '');
                if($Role->save())
                {
                    $roles             = DB::table('roles');
                    $roles->orderBy("roles.id" , "DESC");
                    $roles             = $roles->get();
                    $response['roles'] = $roles;
                    Session::flash('message', 'Role successfully created!');
                    Session::flash('alert-class', 'alert-success');
                    return back();
                }
                else{
                        Session::flash('message', 'Role could not created!');
                        Session::flash('alert-class', 'alert-danger');
                        return back();
                }
        }
    }

    public function show(){

        $show = Role::all();

        return view('dashboard.role.index')->with('role', $show);
    }

    public function edit(Request $request){

        $id    = $request->id;
        $role = Role::find($id);
//        $breadCrum =  BreadCrumHelper::getBreadCrum('role-edit');
        return view('admin.role.edit')->with(['edit' => $role]);


    }
    public  function update(Request $request,$id){
        $request->validate([
            "name"=>"required",
            "display_name" =>"required ",
            "description"=>"required",
        ]);
        $roles               = Role::find($id);
        $roles->name         = $request->input('name');
        $roles->display_name = $request->input('display_name');
        $roles->description  = $request->input('description');
        $roles ->save();
        return redirect('/role')->with('update', $roles);

    }
    public function delete(Request $request){

        if(Role::where('id',"=",$request->id)->delete()){

                Session::flash('alert-class', 'alert-danger');
                Session::flash('message', 'Successfully deleted!');
                return back();

        }else{

                Session::flash('alert-class', 'alert-danger');
                Session::flash('message', 'Could not deleted');
                return back();

        }
    }
}
