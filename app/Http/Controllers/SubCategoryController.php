<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\SubCategory;
use Illuminate\Support\Str;
use Session;
class SubCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        //
          $subCategories  =  SubCategory::all();
          return view('admin.category.sub-category.listing')->with(['subCategories' => $subCategories]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
         $categories= Category::all();
         return view('admin.category.sub-category.create')->with(['categories' => $categories]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        //

        $id  =  $request->id;
        $categories= Category::all();
        $subcategory = SubCategory::where('id', $id)->first();
        return view('admin.category.sub-category.edit')->with(['subcategory' => $subcategory, 'categories' => $categories]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        //
        $id = $request->id;
        $category = SubCategory::where('id', $id)->first();

        if(!empty($category)){
            try {
                $category->delete();
                // Closures include ->first(), ->get(), ->pluck(), etc.
            } catch(\Illuminate\Database\QueryException $ex){

                if($ex->errorInfo['1'] == 1451){
                    session::flash('alert-class',"alert-danger");
                    session::flash('message', "Can not Delete it, because this category used in Child Category");
                    return back();
                }


                // Note any method of class PDOException can be called on $ex.
            }

            session::flash('alert-class',"alert-danger");
            session::flash('message','Category Deleted!');
            return back();

        }else{
            session::flash('alert-class',"alert-danger");
            session::flash('message','Category not found!');
            return back();
        }
    }

    public function save(Request $request){
//         dd($request->all());
         $dataSubCategory = [];

         if(!empty($request->id)){
             $dataSubCategory = [
                 'sub_name' => $request->name,
                 'sub_slug' => Str::slug($request->name),
                 'category_id'     => $request->category_id
             ];
             $subCategory = SubCategory::where('id', $request->id)->first();
             $subCategory->update($dataSubCategory);

             session::flash('alert-class',"alert-success");
             session::flash('message','Sub-Category Updated Successfully!');
             return back();
         }else{
             $dataSubCategory = [
                 'sub_name' => $request->name,
                 'sub_slug' => Str::slug($request->name),
                 'category_id'     => $request->category_id
             ];

             SubCategory::insert($dataSubCategory);
             session::flash('alert-class',"alert-success");
             session::flash('message','Sub-Category Created Successfully!');
             return back();
         }
    }

}
