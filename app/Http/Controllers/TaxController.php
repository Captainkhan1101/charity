<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tax;
use App\USState;
use Session;
class TaxController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        

          $taxes = Tax::with(['UsState'])->get();


        return view('admin.tax.listing')->with(['taxes' => $taxes]);


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $UsStates = USState::all();
        return view('admin.tax.create')->with(['UsStates' => $UsStates]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        if(!empty($request->id)){

        $dataTax = [];
        $dataTax = [

        'tax' => $request->tax,
        'us_state_id' => $request->us_state_id,

        ];
            $tax = Tax::findOrFail($request->id);
           $input = $request->all();
           $tax->fill($input)->save();
            session::flash('alert-class',"alert-success");
            session::flash('message','Tax Updated Successfully!');
            return back();
        }else{
            $dataTax = [];
        $dataTax = [

        'tax' => $request->tax,
        'us_state_id' => $request->us_state_id,

        ];

        Tax::insert($dataTax);
            session::flash('alert-class',"alert-success");
            session::flash('message','Tax Created Successfully!');
            return back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        //
            $id = $request->id;
           $tax = Tax::where('id' , $id)->first();
           $UsStates = USState::all();
           return view('admin.tax.edit')->with(['tax' => $tax, 'UsStates' => $UsStates]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        //
           $id = $request->id;
           $tax  = Tax::where('id', $id)->first();
           if(!empty($tax)){
               $tax->delete();
               session::flash('alert-class',"alert-success");
               session::flash('message','Tax Deleted Successfully!');
               return back();
           }

           session::flash('alert-class', 'alert-danger');
           session::flash('message', 'Data not found!');
           return back();

    }
}
