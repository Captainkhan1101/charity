<?php

namespace App\Http\Controllers;
use App\Models\Role;
use App\RoleUser;
use App\User;
use Illuminate\Http\Request;
use DB;
use Session;
use App\Http\Controllers\Auth\ForgotPasswordController;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
         $users = user::all();
        return view('admin.user-management.listing')->with(['users' => $users]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $roles = Role::all();
        return view('admin.user-management.create')->with(['roles' => $roles]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//        dd($request->all());

       if(!empty($request->id)){
           User::where('id', '=', $request->id)->update([
               'name' => $request->name,


           ]);
           $user = User::find($request->id);


           $role = Role::where('id',"=", $request->role_id)->first();
           RoleUser::where('user_id', '=', $request->id)->delete();
           $user->attachRole($role);

           session::flash('alert-class',"alert-success");
           session::flash('message','User Updated Successfully!');
           return back();
    }else{
        $request->password = 'system123';
        User::insert([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password),
        ]);

        $userID =  DB::getPdo()->lastInsertId();

        $user = User::find($userID);


        $role = Role::where('id',"=", $request->role_id)->first();

        $user->attachRole($role);

        session::flash('alert-class',"alert-success");
        session::flash('message','User Created Successfully!');
        return back();
    }


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        //

        $id = $request->id;
        $user = User::where('id', $id)->with(['roleUser' => function($q) {
            $q->with(['role']);
        } ])->first();
        $roles = Role::all();
//          dd($user->roleUser->role_id);

        return view('admin.user-management.edit')->with(['roles' => $roles, 'user' =>$user ]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
        //
        $id = $request->id;
        $user = User::where('id', $id)->first();

        if(!empty($user)){
            try {

                $user->delete();
                RoleUser::where('user_id', '=', $request->id)->delete();

                // Closures include ->first(), ->get(), ->pluck(), etc.
            } catch(\Illuminate\Database\QueryException $ex){

                if($ex->errorInfo['1'] == 1451){
                    session::flash('alert-class',"alert-danger");
                    session::flash('message', "Can not Delete it");
                    return back();
                }


                // Note any method of class PDOException can be called on $ex.
            }

            session::flash('alert-class',"alert-danger");
            session::flash('message','User Deleted!');
            return back();

        }else{
            session::flash('alert-class',"alert-danger");
            session::flash('message','User not found!');
            return back();
        }
    }
    public function resentPasswordLink(Request $request){
        $response = redirect()->route('admin.user.listing');
        $email = $request->input('email');
        if(!empty($email)){
            (new ForgotPasswordController())->sendForgetPasswordLink($request);
            session::flash('alert-class',"alert-success");
            session::flash('message','Reset Password Link Send Successfully!');
        }
        return $response;
    }
}
