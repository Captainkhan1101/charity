<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class ForgetPasswordRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'sometimes | exists:App\User,email',
            'code' => 'sometimes | min:6',
            'password' => 'sometimes | min:6|same:confirm_password',
            'confirm_password' => 'sometimes | min:6',
        ];
    }
}
