<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductSaveRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

            //
        if(isset($this->id)){
            $photoRule = '';
        }else{
            $photoRule = 'mimes:jpeg,jpg,png,gif|required|max:10000';
        }

            return [
                'title'             => 'required',
                'photo'             => $photoRule,
                'price'             => 'required',
                'retail_price'      => 'required',
                'sku'               => 'required_with:referral_code,numeric',
                'barcode'           => 'required'

        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'title.required'     => 'Title field is required',
            'photo.required'    => 'Photo field is required',
            'price.required_with'    => 'Price field is required',
            'retail_price.required'     => 'Title field is required',
            'sku.required'    => 'Photo field is required',
            'barcode.required_with'    => 'Price field is required'
        ];
    }
}
