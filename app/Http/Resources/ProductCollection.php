<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\ResourceCollection;

class ProductCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return $this->getData();
    }

    private function getData()
    {
        $data = [];
        foreach($this->collection as $item)
        {
            $data[] = [
                "sku" => $item->sku,
                "barcode" => $item->barcode,
                "title" => $item->title,
                "description" => $item->description,
                "price" => $item->price,
                "image" => $this->getImage($item),
                "quanity" => $item->quanity,
                "availiablity_date" => $item->product_availability_date,
            ];
        }
        return collect($data)->sortBy("sku")->values()->all();
    }
    public function getImage($item){
        return asset('images/placeholder.png');
    }
}
