<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    //
    protected $table = 'products';
    protected $fillable = ['title', 'description', 'price', 'retail_price', 'margin', 'profit', 'sku', 'barcode', 'quantity', 'product_availability_date', 'category_id', 'subcategory_id', 'childcategory_id', 'photo', 'variant', 'variant_combination', 'weight', 'unit', 'width', 'height'];

//    protected $dates = ['deleted_at'];
    public function setTitle($value)
    {
        $this->attributes['title'] = $value;
        return $this;
    }
    public function setDescription($value)
    {
        $this->attributes['description'] = $value;
        return $this;
    }
    public function setPrice($value)
    {
        $this->attributes['price'] = $value;
        return $this;
    }
    public function setRetailPrice($value)
    {
        $this->attributes['retail_price'] = $value;
        return $this;
    }
    public function setMargin($value)
    {
        $this->attributes['margin'] = $value;
        return $this;
    }
    public function setProfit($value)
    {
        $this->attributes['profit'] = $value;
        return $this;
    }
    public function setProductAvailabilityDate($value)
    {
        $this->attributes['product_availability_date'] = $value;
        return $this;
    }
    public function setCategoryId($value)
    {
        $this->attributes['category_id'] = $value;
        return $this;
    }
    public function setSubCategoryId($value)
    {
        $this->attributes['subcategory_id'] = $value;
        return $this;
    }
    public function setChildCategoryId($value)
    {
        $this->attributes['childcategory_id'] = $value;
        return $this;
    }
    public function setPhoto($value)
    {
        $this->attributes['photo'] = $value;
        return $this;
    }


    public function setSku($value)
    {
        $this->attributes['sku'] = $value;
        return $this;
    }
    public function setBarcode($value)
    {
        $this->attributes['barcode'] = $value;
        return $this;
    }
    public function setQuantity($value)
    {
        $this->attributes['quantity'] = $value;
        return $this;
    }
    public function setVariant($value)
    {
        $this->attributes['variant'] = $value;
        return $this;
    }
    public function setVariantCombination($value)
    {
        $this->attributes['variant_combination'] = $value;
        return $this;
    }
    public function setWeight($value)
    {
        $this->attributes['weight'] = $value;
        return $this;
    }
    public function setUnit($value)
    {
        $this->attributes['unit'] = $value;
        return $this;
    }
    public function setWidth($value)
    {
        $this->attributes['width'] = $value;
        return $this;
    }
    public function setheight($value)
    {
        $this->attributes['height'] = $value;
        return $this;
    }


    public function produtsImages(){
        return $this->hasMany(ProductImage::class, 'product_id', 'id');
    }
    public function produtsVariants(){
        return $this->hasMany(ProductVariant::class, 'product_id', 'id');
    }
    public function productCategory(){
        return $this->hasMany(ProductCategory::class, 'product_id', 'id');
    }






}
