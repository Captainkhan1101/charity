<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductCategory extends Model
{
    //product_category
    protected $table = 'product_category';
    protected $fillable = ['product_id', 'category_id'];

}
