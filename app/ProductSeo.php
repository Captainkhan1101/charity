<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductSeo extends Model
{
    //
    protected $table = 'products_seo';
    protected $fillable = ['product_id', 'seo_title', 'seo_description', 'seo_url'];

}
