<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductVariant extends Model
{
    //

    protected $table = 'product_variants';
    protected $fillable = ['product_id', 'title', 'price', 'quantity', 'variant_info' , 'sku', 'barcode'];

    public function setProductId($value)
    {
        $this->attributes['product_id'] = $value;
        return $this;
    }

    public function setSize($value)
    {
        $this->attributes['size'] = $value;
        return $this;
    }
    public function setColor($value)
    {
        $this->attributes['color'] = $value;
        return $this;
    }
    public function setTitle($value)
    {
        $this->attributes['title'] = $value;
        return $this;
    }
    public function setPrice($value)
    {
        $this->attributes['price'] = $value;
        return $this;
    }
    public function setQuantity($value)
    {
        $this->attributes['quantity'] = $value;
        return $this;
    }
    public function setSku($value)
    {
        $this->attributes['sku'] = $value;
        return $this;
    }
    public function setBarcode($value)
    {
        $this->attributes['barcode'] = $value;
        return $this;
    }



}
