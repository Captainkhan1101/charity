<?php

namespace App;
use App\Models\Role;
use Illuminate\Database\Eloquent\Model;

class RoleUser extends Model
{
    //
    protected $table = 'role_user';

    public function role(){

        return $this->belongsTo(Role::class, 'role_id' , 'id');

    }
}
