<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubCategory extends Model
{
    //
    protected $table = 'subcategories';
    protected $fillable = ['category_id', 'sub_name', 'sub_slug'];
}
