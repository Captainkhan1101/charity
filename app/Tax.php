<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tax extends Model
{
    //
    protected $table = 'taxes';
    protected $fillable = ['tax', 'us_state_id'];

    public function UsState(){
        return $this->hasOne(USState::class, 'id', 'us_state_id');
    }
}
