<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class USState extends Model
{
    //
    protected $table = 'us_states';
    protected $fillable = ['name', 'code'];
}
