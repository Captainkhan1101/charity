<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTableProducts4 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function (Blueprint $table) {
            //
            $table->dropColumn('color');
            $table->dropColumn('size');
            $table->string('variant_combination')->nullable();
            $table->string('variant')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products4', function (Blueprint $table) {
            //
            $table->string('color');
            $table->string('size');
            $table->dropColumn('variant_combination')->nullable();
            $table->dropColumn('variant')->nullable();

        });
    }
}
