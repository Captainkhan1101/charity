function isEmpty(value) {
    let response = true;
    if (value != null && value != 'null' && value != 'undefined' && value != '') {
        response = false;
    }
    return response;
}
function canCalculate(price,cost) {
    return !isEmpty(price.val()) && !isEmpty(cost.val());
}
function calculateProfit(price,cost) {
    return (parseFloat(price) - parseFloat(cost)).toFixed(2);
}
function calculateMargin(profit,cost) {
    return ((profit/cost) * 100).toFixed(2);
}
function showOrHideMultiVariants() {
    if($('#multiple-variant:checkbox:checked').length > 0){
        $('#product-multi-variant-container').show();
    }else{
        $('#product-multi-variant-container').hide();
    }
}
function calculatePrice() {
    let price = $('#product-price');
    let cost_per_item = $('#cost-per-item');
    let margin = $('#calculated-margin');
    let profit = $('#calculated-profit');
    if(canCalculate(price,cost_per_item)){
        let calculated_profit = calculateProfit(price.val(),cost_per_item.val());
        profit.html('Rs ' + calculated_profit);
        margin.html(calculateMargin(calculated_profit,price.val()) + '%');
    }
}
function addOption(){
    let options = $('#product-multi-variant-options');
}

$(document).ready(function () {
    if($("#elm1").length > 0){
        tinymce.init({
            selector: "textarea#elm1",
            theme: "modern",
            height:300,
            plugins: [
                "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
                "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
                "save table contextmenu directionality emoticons template paste textcolor"
            ],
            toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | l      ink image | print preview media fullpage | forecolor backcolor emoticons",
            style_formats: [
                {title: 'Bold text', inline: 'b'},
                {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
                {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
                {title: 'Example 1', inline: 'span', classes: 'example1'},
                {title: 'Example 2', inline: 'span', classes: 'example2'},
                {title: 'Table styles'},
                {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
            ]
        });
    }
    $('.datepicker-auto-close').datepicker({
        autoclose: true,
        todayHighlight: true
    });
});
function refreshAvailableForm() {
    $('#available-date-form').trigger('reset');
    $('#publish-date-div').hide();
}

// $(document).ready(function(){
//
//     // Add new element
//     $('.add_field_button').click(function(){
//
//         // Finding total number of elements added
//         var total_element = $("product-element").length;
//
//         // last <div> with element class id
//         var lastid = $(".product-element:last").attr("id");
//         var split_id = lastid.split("_");
//         var nextindex = Number(split_id[1]) + 1;
//
//         var max = 3;
//         // Check total number elements
//         if(total_element < max ){
//             // Adding new div container after last occurance of element class
//             $(".product-element:last").after(`<div class='product-element' id='div_"+ nextindex +"'>
//
// </div>`);
//
//             // Adding element to <div>
//             $("#div_" + nextindex).append(`
//                                                 <div class="row">
//                                                     <div class="col-3">
//                                                         <select name="tst" class="form-control">
//                                                             <option selected></option>
//                                                             <option>size</option>
//                                                             <option>color</option>
//                                                         </select>
//                                                     </div>
//                                                     <div class="col-8">
//                                                         <input type="text" value="Amsterdam,Washington,Sydney,Beijing,Cairo" data-role="tagsinput" />
//                                                     </div>&nbsp;<span id='remove_` + nextindex + `' class='remove'>X</span>
//                                                </div>
//                                             `);
//
//         }
//
//     });
//
//     // Remove element
//     $('.container').on('click','.remove',function(){
//
//         var id = this.id;
//         var split_id = id.split("_");
//         var deleteindex = split_id[1];
//
//         // Remove <div> with id
//         $("#div_" + deleteindex).remove();
//
//     });
// });
