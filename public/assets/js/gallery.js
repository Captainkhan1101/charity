function uploadclick(){
    $("#uploadFile").click();
    $("#uploadFile").change(function(event) {
        readURL(this);
        $("#uploadTrigger").html($("#uploadFile").val());
    });
}
function readURL(input){
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#adminimg').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    }
}
function uploadclick1(){
    $("#uploadFile1").click();
    $("#uploadFile1").change(function(event) {
        readURL1(this);
        $("#uploadTrigger1").html($("#uploadFile1").val());
    });
}

function readURL1(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#adminimg1').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    }
}

function uploadclick2(){
    $("#uploadFile2").click();
    $("#uploadFile2").change(function(event) {
        readURL2(this);
        $("#uploadTrigger2").html($("#uploadFile2").val());
    });

}

function readURL2(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
        }
        reader.readAsDataURL(input.files[0]);
    }
}

function uploadclick3(){
    $("#uploadFile3").click();
    $("#uploadFile3").change(function(event) {
        readURL3(this);
        $("#uploadTrigger3").html($("#uploadFile3").val());
    });

}

function readURL3(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#adminimg3').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    }
}

function uploadclick4(){
    $("#uploadFile4").click();
    $("#uploadFile4").change(function(event) {
        readURL4(this);
        $("#uploadTrigger4").html($("#uploadFile4").val());
    });

}

function readURL4(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
        }
        reader.readAsDataURL(input.files[0]);
    }
}
// Gallery Section

$(document).on('click', '.close1' ,function() {
    var id = $(this).find('input[type=hidden]').val();
    $('#galval1'+id).remove();
    $(this).parent().parent().remove();
});

$(document).on('click', '#prod_gallery1' ,function() {
    $('#uploadgallery1').click();
    $('#gallery-wrap1 .row').html('');
    $('#form1').find('.removegal1').val(0);
});

$("#uploadgallery1").change(function(){
    var total_file=document.getElementById("uploadgallery1").files.length;
    for(var i=0;i<total_file;i++)
    {
        $('#gallery-wrap1 .row').append('<div class="col-sm-4">'+
            '<div class="gallery__img">'+
            '<img src="'+URL.createObjectURL(event.target.files[i])+'" alt="gallery image">'+
            '<div class="gallery-close close1">'+
            '<input type="hidden" value="'+i+'">'+
            '<i class="fa fa-close"></i>'+
            '</div>'+
            '</div>'+
            '</div>');
        $('#form1').append('<input type="hidden" name="galval[]" id="galval1'+i+'" class="removegal1" value="'+i+'">')
    }

});

$(document).on('click', '.close2' ,function() {
    var id = $(this).find('input[type=hidden]').val();
    $('#galval2'+id).remove();
    $(this).parent().parent().remove();
});

$(document).on('click', '#prod_gallery2' ,function() {
    $('#uploadgallery2').click();
    $('#gallery-wrap2 .row').html('');
    $('#form2').find('.removegal2').val(0);
});

$("#uploadgallery2").change(function(){
    var total_file=document.getElementById("uploadgallery2").files.length;
    for(var i=0;i<total_file;i++)
    {
        $('#gallery-wrap2 .row').append('<div class="col-sm-4">'+
            '<div class="gallery__img">'+
            '<img src="'+URL.createObjectURL(event.target.files[i])+'" alt="gallery image">'+
            '<div class="gallery-close close2">'+
            '<input type="hidden" value="'+i+'">'+
            '<i class="fa fa-close"></i>'+
            '</div>'+
            '</div>'+
            '</div>');
        $('#form2').append('<input type="hidden" name="galval[]" id="galval2'+i+'" class="removegal2" value="'+i+'">')
    }

});

$(document).on('click', '.close3' ,function() {
    var id = $(this).find('input[type=hidden]').val();
    $('#galval3'+id).remove();
    $(this).parent().parent().remove();
});

$(document).on('click', '#prod_gallery3' ,function() {
    $('#uploadgallery3').click();
    $('#gallery-wrap3 .row').html('');
    $('#form3').find('.removegal3').val(0);
});

$("#uploadgallery3").change(function(){
    var total_file=document.getElementById("uploadgallery3").files.length;
    for(var i=0;i<total_file;i++)
    {
        $('#gallery-wrap3 .row').append('<div class="col-sm-4">'+
            '<div class="gallery__img">'+
            '<img src="'+URL.createObjectURL(event.target.files[i])+'" alt="gallery image">'+
            '<div class="gallery-close close3">'+
            '<input type="hidden" value="'+i+'">'+
            '<i class="fa fa-close"></i>'+
            '</div>'+
            '</div>'+
            '</div>');
        $('#form3').append('<input type="hidden" name="galval[]" id="galval3'+i+'" class="removegal3" value="'+i+'">')
    }

});

function uploadclick4(){
    $("#uploadFile4").click();
    $("#uploadFile4").change(function(event) {
        readURL4(this);
        $("#uploadTrigger4").html($("#uploadFile4").val());
    });

}

function uploadclick3(){
    $("#uploadFile3").click();
    $("#uploadFile3").change(function(event) {
        readURL3(this);
        $("#uploadTrigger3").html($("#uploadFile3").val());
    });

}