
        $(document).ready(function(){
            $("#refes_2").change(function(){
                product_variants();
            });
            
            // Add new element
            $(".add").click(function(){

                // Finding total number of elements added
                var total_element = $(".element").length;

                // last <div> with element class id
                var lastid = $(".element:last").attr("id");
                var split_id = lastid.split("_");
                var nextindex = Number(split_id[1]) + 1;

                var max = 3;
                // Check total number elements
                if(total_element < max ){
                    // Adding new div container after last occurance of element class
                    $(".element:last").after("<div class='element' id='div_"+ nextindex +"'></div>");

                    // Adding element to <div>
                    $("#div_" + nextindex).append('<h4 class="mt-0 header-title mb-2"><strong>Option '+nextindex+' </strong></h4><div id="product-multi-variant-options" class="product-element"> <div class="row"> <div class="col-3"> <div class="autocomplete" > <input id="myInput'+nextindex+'" type="text" class="form-control" name="variant[]" placeholder="Color" > </div> </div> <div class="col-8" style="margin-right: -12px!important;"> <input type="text" name="variant_add[' + nextindex + ']" onchange="product_variants()" id="refes_' + nextindex + '" value="" data-role="tagsinput" /> </div><span id="remove_' + nextindex + '" class="remove" >Remove</span></div><hr></div>');
                    $('#refes_'+ nextindex).tagsinput('refresh');


                    /*An array containing all the country names in the world:*/
                    var countries = ["Size","Color","Title","Meterial","Style"];

                    /*initiate the autocomplete function on the "myInput" element, and pass along the countries array as possible autocomplete values:*/
                    autocomplete(document.getElementById("myInput" + nextindex), countries);
                }
                
            // Remove element
            $('.container').on('click','.remove',function(){

                var id = this.id;
                var split_id = id.split("_");
                var deleteindex = split_id[1];

                // Remove <div> with id
                $("#div_" + deleteindex).remove();
                product_variants();

            });
        });
    });
    function removeVariantRow(index){
        $('#row-variant'+index).remove();
    }
    function unique_find(data) {
        var uniqueNames = [];
        $.each(data, function(i, el){
            if($.inArray(el, uniqueNames) === -1 && el != null && el !== '') uniqueNames.push(el);
        });
        return uniqueNames;
    }
    function product_variants() {
        var variant_add1 = $("input[name='variant_add[0]']")
            .map(function(){return $(this).val();}).get();
        var variant_add2 = $("input[name='variant_add[2]']")
            .map(function(){return $(this).val();}).get();
        var variant_add3 = $("input[name='variant_add[3]']")
            .map(function(){return $(this).val();}).get();
        if(variant_add1){
            var variant_add1str   =   variant_add1.join(", ");
            variant_add1str           =   variant_add1str.replace(/\s/g, '');
            var variant_arry  = variant_add1str.split(',');
            variant_add1  =   unique_find(variant_arry);
        }else{
            variant_add1 = [];
        }
        if(variant_add2){
            var variant_add2str   =   variant_add2.join(", ");
            variant_add2str           =   variant_add2str.replace(/\s/g, '');
            var variant_arry  = variant_add2str.split(',');
            variant_add2  =   unique_find(variant_arry);
        }else{
            variant_add2 = [];
        }
        variant_add3  =   unique_find(variant_add3);
        var combos = [""];
        if(isEmpty(variant_add3) && isEmpty(variant_add2)){
            var variant_add1str   =   variant_add1.join(", ");
            variant_add1str           =   variant_add1str.replace(/\s/g, '');
            var variant_arry  = variant_add1str.split(',');
            variant_add1  =   unique_find(variant_arry);
            combos= variant_add1;
        }else if( !isEmpty(variant_add3) && !isEmpty(variant_add2) && !isEmpty(variant_add1)  ){
            var a = [variant_add1, variant_add2, variant_add3];
            for (var i=0; i<a.length; i++) { // and repeatedly
                var ai = a[i],
                    l = ai.length;
                combos = $.map(combos, function(r) { // make result a new array of
                    var ns = []; // new combinations of
                    for (var j=0; j<l; j++) // each of the letters in ai
                        ns[j] = r + ai[j]+'/'; // and the old results
                    return ns;
                }); // using the odds of jQuery.map with returned arrays
            }
        }else{
            for(var i = 0; i < variant_add1.length; i++)
            {
                for(var j = 0; j < variant_add2.length; j++)
                {
                    combos.push(variant_add1[i] + '/'+ variant_add2[j])
                }
            }
        }
        combos = combos.filter(function(v){return v!==''});
        var  tablehtml = '' ;
        $.each(combos, function (key, value) {
            tablehtml += '<tr id="row-variant'+key+'"><td>'+value+' <input name="variant_info[]" type="hidden" value="'+value+'"></td><td><input type="number" name="v_price[]" style="width: 104px;"  value="" min="0"></td><td><input type="number" name="v_quantity[]" style="width: 104px;" value="" min="0"></td><td><input type="text" name="v_sku[]" style="width: 104px;" value=""></td><td><input type="text" name="v_barcode[]" style="width: 104px;" value=""></td><td> <button class="btn btn-danger btn-sm ui-close1" style="font-size: 12px; width: 59px;" onclick="removeVariantRow('+key+');" > remove</button></td></tr>';
        })
        $('#preview-variant').html(tablehtml);
    }