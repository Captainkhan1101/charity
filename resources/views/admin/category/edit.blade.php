@extends('layouts.admin-layouts')
@push('styles')
    <!-- Dropzone css -->
    <link href="{{asset('assets/plugins/dropzone/dist/dropzone.css')}}" rel="stylesheet" type="text/css">
    <!-- DatePicker css -->
    <link href="{{asset('assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css')}}" rel="stylesheet">
    <link href="{{ asset('assets/css/bootstrap-colorpicker.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/image-css/style.css') }}" rel="stylesheet">
    {{--    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.5/css/bootstrap.min.css">--}}
    {{--    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha512-MoRNloxbStBcD8z3M/2BmnT+rg4IsMxPkXaGh2zD6LGNNFE80W3onsAhRcMAMrSoyWL9xD7Ert0men7vR8LUZg==" crossorigin="anonymous" />--}}
    <link href="https://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootswatch/4.3.1/flatly/bootstrap.min.css">
    <link rel="stylesheet" href="{{ asset('assets/css/tagsinput.css') }}">

    <style>
        .badge { margin: 2px 5px; }
    </style>
    //atuo complete jquery

    <style>
        * {
            box-sizing: border-box;
        }

        /*body {*/
        /*    font: 16px Arial;*/
        /*}*/

        /*the container must be positioned relative:*/
        .autocomplete {
            position: relative;
            display: inline-block;
        }

        /*input {*/
        /*    border: 1px solid transparent;*/
        /*    background-color: #f1f1f1;*/
        /*    padding: 10px;*/
        /*    font-size: 16px;*/
        /*}*/

        /*input[type=text] {*/
        /*    background-color: #f1f1f1;*/
        /*    width: 100%;*/
        /*}*/

        /*input[type=submit] {*/
        /*    background-color: DodgerBlue;*/
        /*    color: #fff;*/
        /*    cursor: pointer;*/
        /*}*/

        .autocomplete-items {
            position: absolute;
            border: 1px solid #d4d4d4;
            border-bottom: none;
            border-top: none;
            z-index: 99;
            /*position the autocomplete items to be the same width as the container:*/
            top: 100%;
            left: 0;
            right: 0;
        }

        .autocomplete-items div {
            padding: 10px;
            cursor: pointer;
            background-color: #fff;
            border-bottom: 1px solid #d4d4d4;
        }

        /*when hovering an item:*/
        .autocomplete-items div:hover {
            background-color: #e9e9e9;
        }

        /*when navigating through the items using the arrow keys:*/
        .autocomplete-active {
            background-color: DodgerBlue !important;
            color: #ffffff;
        }
    </style>



@endpush
@section('content')
    <div class="page-content-wrapper ">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">



                    <div class="float-right page-breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ route('home') }}">Dashboard</a></li>
                            <li class="breadcrumb-item"><a href="{{route('admin.category.listing')}}">Collection</a></li>
                            <li class="breadcrumb-item active">Edit-Collection</li>
                        </ol>
                    </div>
                    <h5 class="page-title">Edit Collection</h5>
                </div>
                <!-- end row -->
                <div class="container" id="add-product-form" action="javascript:void(0)">
                    @if(!empty( \Illuminate\Support\Facades\Session::get('message') ) || $errors->any())
                        @include('error-messages')
                    @endif
                    <form  action="{{ route('admin.category.save') }}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="row">
                            <div class="col-md-8">
                                <div class="card m-b-30">
                                    <div class="card-body">
                                        <div class="form-group">
                                            <label>Title</label>
                                            <input type="text" name="title" class="form-control"  value="{{ $category->name }}" placeholder="Laptop 10th Generation" required />
                                        </div>
                                        <div class="form-group">
                                            <label>Description</label>
                                            <textarea id="elm1" name="description" value="{{ $category->description }}"  placeholder="Type something..."></textarea>
                                        </div>
                                    </div>
                                </div>


                                <div class="card m-b-30">
                                    <div class="card-body">

                                        <h6 class="mt-0 header-title mb-4">
                                            Search engine listing preview
                                            <i class="fa fa-edit float-right cursor-pointer" onclick="$('#publish-seo-div').toggle()">Edit website SEO</i>
                                        </h6>
                                        <h6 class="mt-0 header-title mb-4">
                                            Add a title and description to see how this product might appear in a search engine listing
                                        </h6>
                                        <div id="publish-seo-div" style="display:none;">
                                            <div class="row">
                                                <div class="col-12 mt-4">
                                                    <h5 class="mt-0 header-title">Page Title</h5>
                                                    <input type="text" name="seo_title" class="form-control"  value="{{ !empty($category->categorySeo)? $category->categorySeo->seo_title : '' }}" placeholder="Enter Title"/>
                                                </div>
                                                <div class="col-12 mt-4">
                                                    <h5 class="mt-0 header-title">Description</h5>
                                                    <textarea  name="seo_description" class="form-control"  value="{{ !empty($category->categorySeo)? $category->categorySeo->seo_description : '' }}" >{{ !empty($category->categorySeo)? $category->categorySeo->seo_description : '' }}
                                                    </textarea>

                                                </div>
                                                <div class="col-12 mt-4">
                                                    <h5 class="mt-0 header-title">URL and handle</h5>
                                                    <input type="text" name="seo_url" class="form-control"  value="{{ !empty($category->categorySeo)? $category->categorySeo->seo_url : '' }}" placeholder="https://www.system.com/" />


                                                </div>
                                            </div>


                                        </div>
                                    </div>
                                    <!-- end col -->


                                </div>

                            </div><!-- end col -->
                            <div class="col-md-4">
                                <div class="card m-b-30">

                                    <div class="card-body pb-1">

                                        <div class="form-group">
                                            <label class="control-label col-sm-4 col-lg-12" for="edit_profile_photo">Collection image</label>
                                            <div class="col-sm-6 col-lg-12">
                                                <input type="file" id="uploadFile3" class="hidden" style="visibility: hidden;" name="photo" value="">
                                                <button type="button" id="uploadTrigger3" onclick="uploadclick3()" class="form-control"><i class="fa fa-download"></i> Choose Image</button>
                                                <p class="text-center">Prefered Size: (600x600) or Square Sized Image</p>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div> <!-- end col -->

                        </div> <!-- end row -->
                        <button type="submit" class="btn btn-primary pull-right">Submit</button>
                    </form>
                </div>
            </div>
        </div>
        @endsection
        @push('scripts')


            <script src="{{ asset('assets/js/tagsinput.js') }}"></script>
        {{--    <script src="{{ asset('assets/js/bootstrap-tagsinput-angular.min.js') }}" ></script>--}}
        <!-- Parsley js -->
            <script src="{{asset('assets/plugins/parsleyjs/parsley.min.js')}}"></script>
            <!--Wysiwig js-->
            <script src="{{asset('assets/plugins/tinymce/tinymce.min.js')}}"></script>
            <!-- Dropzone js -->
            <script src="{{asset('assets/plugins/dropzone/dist/dropzone.js')}}"></script>
            <!-- DatePicker js -->
            <script src="{{asset('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"></script>
            <script>
                $(document).ready(function() {
                    $('#add-product-form').parsley();
                });
            </script>
            <script src="{{  asset('assets/js/bootstrap-colorpicker.js') }}"></script>
            <script src="{{  asset('assets/js/bootstrap-colorpicker.min.js') }}"></script>
            <script>
                (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
                })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

                ga('create', 'UA-42755476-1', 'bootstrap-tagsinput.github.io');
                ga('send', 'pageview');
            </script>

            <script>

                function isEmpty(value) {
                    let response = true;
                    if (value != null && value != 'null' && value != 'undefined' && value != '') {
                        response = false;
                    }
                    return response;
                }
                $(document).ready(function(){
                    // $(".variant-inp").keyup(function(){
                    //     product_variants();
                    // });
                    $("input[name='variant_add[0]").change(function(){

                        product_variants();
                    });
                    $("#refes_2").change(function(){

                        product_variants();
                    });
                    $("input[name='variant_add[3]").change(function(){

                        product_variants();
                    });
                });



                // $(document).ready(function() {
                //     var max_fields      = 3; //maximum input boxes allowed
                //     var wrapper   		= $(".input_fields_wrap"); //Fields wrapper
                //     var add_button      = $(".add_field_button"); //Add button ID
                //
                //     var x = 1; //initlal text box count
                //     $(add_button).click(function(e){ //on add input button click
                //         e.preventDefault();
                //         if(x < max_fields){ //max input box allowed
                //             x++; //text box increment
                //             $(wrapper).append('<div id="product-multi-variant-options"> <div class="row"> <div class="col-3"> <select name="tst" class="form-control"> <option selected></option> <option>size</option> <option>color</option> </select> </div> <div class="col-8"> <input type="text" value="Amsterdam,Washington,Sydney,Beijing,Cairo" data-role="tagsinput" /> </div> </div> </div>'); //add input box
                //         }
                //         $('input').tagsinput('refresh');
                //     });
                //
                //     $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
                //         e.preventDefault(); $(this).parent('div').remove(); x--;
                //     })
                // });




                // Remove parent of 'remove' link when link is clicked.
                // $('.input_fields_wrap').on('click', function(e) {
                //     e.preventDefault();
                //
                //     $(this).parent().remove();
                // });







                $(document).ready(function(){

                    // Add new element
                    $(".add").click(function(){

                        // Finding total number of elements added
                        var total_element = $(".element").length;

                        // last <div> with element class id
                        var lastid = $(".element:last").attr("id");
                        var split_id = lastid.split("_");
                        var nextindex = Number(split_id[1]) + 1;

                        var max = 3;
                        // Check total number elements
                        if(total_element < max ){
                            // Adding new div container after last occurance of element class
                            $(".element:last").after("<div class='element' id='div_"+ nextindex +"'></div>");

                            // Adding element to <div>
                            $("#div_" + nextindex).append('<h4 class="mt-0 header-title mb-2"><strong>Option '+nextindex+' </strong></h4><div id="product-multi-variant-options" class="product-element"> <div class="row"> <div class="col-3"> <div class="autocomplete" > <input id="myInput'+nextindex+'" type="text" class="form-control" name="variant[]" placeholder="Color" > </div> </div> <div class="col-8" style="margin-right: -12px!important;"> <input type="text" name="variant_add[' + nextindex + ']" id="refes_' + nextindex + '" value="" data-role="tagsinput" /> </div><span id="remove_' + nextindex + '" class="remove" >Remove</span></div><hr></div>');
                            $('#refes_'+ nextindex).tagsinput('refresh');


                            /*An array containing all the country names in the world:*/
                            var countries = ["Size","Color","Title","Meterial","Style"];

                            /*initiate the autocomplete function on the "myInput" element, and pass along the countries array as possible autocomplete values:*/
                            autocomplete(document.getElementById("myInput" + nextindex), countries);
                        }

                    });

                    // Remove element
                    $('.container').on('click','.remove',function(){

                        var id = this.id;
                        var split_id = id.split("_");
                        var deleteindex = split_id[1];

                        // Remove <div> with id
                        $("#div_" + deleteindex).remove();

                    });
                });




            </script>


            <script>


                $('.colorpicker-component').colorpicker();
                $('.colorpick').colorpicker();
                $("#check3").change(function() {
                    if(this.checked) {
                        $("#fimg1").show();
                    }
                    else
                    {
                        $("#fimg1").hide();

                    }
                });
            </script>
            <script type="text/javascript">


                $(document).on('click','#add-color',function() {

                    $(".color-area").append('<div class="form-group single-color">'+
                        ' <label class="control-label col-sm-4" for="blood_group_display_name">'+
                        ' Product Colors* <span>(Choose Your Favourite Color.)</span></label>'+
                        '<div class="col-sm-6">'+
                        '<div class="input-group colorpicker-component">'+
                        '<input  type="text" name="color[]" value="#000000"  class="form-control colorpick"  onchange="variantColorPick()";  />'+
                        '<span class="input-group-addon"><i></i></span>'+
                        '<span class="ui-close1">X</span>'+
                        '</div>'+
                        '</div>'+
                        '</div>');
                    $('.colorpicker-component').colorpicker();
                    $('.colorpick').colorpicker();

                });




                $(document).on('click', '.ui-close1' ,function() {
                    $(this.parentNode.parentNode.parentNode).hide();
                    $(this.parentNode.parentNode.parentNode).remove();
                    product_variants();
                    if (isEmpty($('#q1'))) {

                        $(".color-area").append('<div class="form-group single-color">'+
                            ' <label class="control-label col-sm-4" for="blood_group_display_name">'+
                            ' Product Colors* <span>(Choose Your Favourite Color.)</span></label>'+
                            '<div class="col-sm-6">'+
                            '<div class="input-group colorpicker-component">'+
                            '<input  type="text" name="color[]" value="#000000"  class="form-control colorpick" onchange="variantColorPick()";  />'+
                            '<span class="input-group-addon"><i></i></span>'+
                            '<span class="ui-close1">X</span>'+
                            '</div>'+
                            '</div>'+
                            '</div>');

                        $('.colorpicker-component').colorpicker();
                        $('.colorpick').colorpicker();
                    }
                });
            </script>

            <script type="text/javascript">
                $('#pro-sub').click(function(){



                });
                function removeVariantRow(index){
                    $('#row-variant'+index).remove();
                }

                function unique_find(data) {
                    var uniqueNames = [];
                    $.each(data, function(i, el){
                        if($.inArray(el, uniqueNames) === -1 && el != null && el !== '') uniqueNames.push(el);
                    });
                    return uniqueNames;
                }

                function product_variants() {


                    var variant_add1 = $("input[name='variant_add[0]']")
                        .map(function(){return $(this).val();}).get();
                    var variant_add2 = $("input[name='variant_add[2]']")
                        .map(function(){return $(this).val();}).get();
                    var variant_add3 = $("input[name='variant_add[3]']")
                        .map(function(){return $(this).val();}).get();



                    if(variant_add1){
                        var variant_add1str   =   variant_add1.join(", ");
                        variant_add1str           =   variant_add1str.replace(/\s/g, '');
                        var variant_arry  = variant_add1str.split(',');
                        variant_add1  =   unique_find(variant_arry);
                    }else{
                        variant_add1 = [];

                    }
                    if(variant_add2){
                        var variant_add2str   =   variant_add2.join(", ");
                        variant_add2str           =   variant_add2str.replace(/\s/g, '');
                        var variant_arry  = variant_add2str.split(',');
                        variant_add2  =   unique_find(variant_arry);
                    }else{
                        variant_add2 = [];

                    }


                    variant_add3  =   unique_find(variant_add3);








                    var combos = [""];
                    if(isEmpty(variant_add3) && isEmpty(variant_add2)){
                        var variant_add1str   =   variant_add1.join(", ");
                        variant_add1str           =   variant_add1str.replace(/\s/g, '');
                        var variant_arry  = variant_add1str.split(',');
                        variant_add1  =   unique_find(variant_arry);
                        combos= variant_add1;

                    }else if( !isEmpty(variant_add3) && !isEmpty(variant_add2) && !isEmpty(variant_add1)  )
                    {

                        var a = [variant_add1, variant_add2, variant_add3];


                        for (var i=0; i<a.length; i++) { // and repeatedly
                            var ai = a[i],
                                l = ai.length;
                            combos = $.map(combos, function(r) { // make result a new array of
                                var ns = []; // new combinations of
                                for (var j=0; j<l; j++) // each of the letters in ai
                                    ns[j] = r + ai[j]+'/'; // and the old results
                                return ns;
                            }); // using the odds of jQuery.map with returned arrays
                        }

                    }else
                    {


                        for(var i = 0; i < variant_add1.length; i++)
                        {
                            for(var j = 0; j < variant_add2.length; j++)
                            {
                                combos.push(variant_add1[i] + '/'+ variant_add2[j])
                            }
                        }

                    }







                    combos = combos.filter(function(v){return v!==''});
                    var  tablehtml = '' ;

                    $.each(combos, function (key, value) {

                        tablehtml += '<tr id="row-variant'+key+'"><td>'+value+' <input name="variant_info[]" type="hidden" value="'+value+'"></td><td><input type="number" name="v_price[]" style="width: 104px;"  value="" min="0"></td><td><input type="number" name="v_quantity[]" style="width: 104px;" value="" min="0"></td><td><input type="text" name="v_sku[]" style="width: 104px;" value=""></td><td><input type="text" name="v_barcode[]" style="width: 104px;" value=""></td><td> <button class="btn btn-danger btn-sm ui-close1" style="font-size: 12px; width: 59px;" onclick="removeVariantRow('+key+');" > remove</button></td></tr>';

                    })


                    $('#preview-variant').html(tablehtml);
                }

                $('.colorpick').change(function(event) {
                    product_variants();
                });

                function variantColorPick(){
                    product_variants();
                }



            </script>

            <script type="text/javascript">

                function uploadclick(){
                    $("#uploadFile").click();
                    $("#uploadFile").change(function(event) {
                        readURL(this);
                        $("#uploadTrigger").html($("#uploadFile").val());
                    });
                }


                function readURL(input){
                    if (input.files && input.files[0]) {
                        var reader = new FileReader();
                        reader.onload = function (e) {
                            $('#adminimg').attr('src', e.target.result);
                        }
                        reader.readAsDataURL(input.files[0]);
                    }
                }

                function uploadclick1(){
                    $("#uploadFile1").click();
                    $("#uploadFile1").change(function(event) {
                        readURL1(this);
                        $("#uploadTrigger1").html($("#uploadFile1").val());
                    });

                }

                function readURL1(input) {
                    if (input.files && input.files[0]) {
                        var reader = new FileReader();
                        reader.onload = function (e) {
                            $('#adminimg1').attr('src', e.target.result);
                        }
                        reader.readAsDataURL(input.files[0]);
                    }
                }

                function uploadclick2(){
                    $("#uploadFile2").click();
                    $("#uploadFile2").change(function(event) {
                        readURL2(this);
                        $("#uploadTrigger2").html($("#uploadFile2").val());
                    });

                }

                function readURL2(input) {
                    if (input.files && input.files[0]) {
                        var reader = new FileReader();
                        reader.onload = function (e) {
                        }
                        reader.readAsDataURL(input.files[0]);
                    }
                }

                function uploadclick3(){
                    $("#uploadFile3").click();
                    $("#uploadFile3").change(function(event) {
                        readURL3(this);
                        $("#uploadTrigger3").html($("#uploadFile3").val());
                    });

                }

                function readURL3(input) {
                    if (input.files && input.files[0]) {
                        var reader = new FileReader();
                        reader.onload = function (e) {
                            $('#adminimg3').attr('src', e.target.result);
                        }
                        reader.readAsDataURL(input.files[0]);
                    }
                }

                function uploadclick4(){
                    $("#uploadFile4").click();
                    $("#uploadFile4").change(function(event) {
                        readURL4(this);
                        $("#uploadTrigger4").html($("#uploadFile4").val());
                    });

                }

                function readURL4(input) {
                    if (input.files && input.files[0]) {
                        var reader = new FileReader();
                        reader.onload = function (e) {
                        }
                        reader.readAsDataURL(input.files[0]);
                    }
                }

            </script>

            <script>
                // Gallery Section

                $(document).on('click', '.close1' ,function() {
                    var id = $(this).find('input[type=hidden]').val();
                    $('#galval1'+id).remove();
                    $(this).parent().parent().remove();
                });

                $(document).on('click', '#prod_gallery1' ,function() {
                    $('#uploadgallery1').click();
                    $('#gallery-wrap1 .row').html('');
                    $('#form1').find('.removegal1').val(0);
                });

                $("#uploadgallery1").change(function(){
                    var total_file=document.getElementById("uploadgallery1").files.length;
                    for(var i=0;i<total_file;i++)
                    {
                        $('#gallery-wrap1 .row').append('<div class="col-sm-4">'+
                            '<div class="gallery__img">'+
                            '<img src="'+URL.createObjectURL(event.target.files[i])+'" alt="gallery image">'+
                            '<div class="gallery-close close1">'+
                            '<input type="hidden" value="'+i+'">'+
                            '<i class="fa fa-close"></i>'+
                            '</div>'+
                            '</div>'+
                            '</div>');
                        $('#form1').append('<input type="hidden" name="galval[]" id="galval1'+i+'" class="removegal1" value="'+i+'">')
                    }

                });

                $(document).on('click', '.close2' ,function() {
                    var id = $(this).find('input[type=hidden]').val();
                    $('#galval2'+id).remove();
                    $(this).parent().parent().remove();
                });

                $(document).on('click', '#prod_gallery2' ,function() {
                    $('#uploadgallery2').click();
                    $('#gallery-wrap2 .row').html('');
                    $('#form2').find('.removegal2').val(0);
                });

                $("#uploadgallery2").change(function(){
                    var total_file=document.getElementById("uploadgallery2").files.length;
                    for(var i=0;i<total_file;i++)
                    {
                        $('#gallery-wrap2 .row').append('<div class="col-sm-4">'+
                            '<div class="gallery__img">'+
                            '<img src="'+URL.createObjectURL(event.target.files[i])+'" alt="gallery image">'+
                            '<div class="gallery-close close2">'+
                            '<input type="hidden" value="'+i+'">'+
                            '<i class="fa fa-close"></i>'+
                            '</div>'+
                            '</div>'+
                            '</div>');
                        $('#form2').append('<input type="hidden" name="galval[]" id="galval2'+i+'" class="removegal2" value="'+i+'">')
                    }

                });

                $(document).on('click', '.close3' ,function() {
                    var id = $(this).find('input[type=hidden]').val();
                    $('#galval3'+id).remove();
                    $(this).parent().parent().remove();
                });

                $(document).on('click', '#prod_gallery3' ,function() {
                    $('#uploadgallery3').click();
                    $('#gallery-wrap3 .row').html('');
                    $('#form3').find('.removegal3').val(0);
                });

                $("#uploadgallery3").change(function(){
                    var total_file=document.getElementById("uploadgallery3").files.length;
                    for(var i=0;i<total_file;i++)
                    {
                        $('#gallery-wrap3 .row').append('<div class="col-sm-4">'+
                            '<div class="gallery__img">'+
                            '<img src="'+URL.createObjectURL(event.target.files[i])+'" alt="gallery image">'+
                            '<div class="gallery-close close3">'+
                            '<input type="hidden" value="'+i+'">'+
                            '<i class="fa fa-close"></i>'+
                            '</div>'+
                            '</div>'+
                            '</div>');
                        $('#form3').append('<input type="hidden" name="galval[]" id="galval3'+i+'" class="removegal3" value="'+i+'">')
                    }

                });

                function uploadclick4(){
                    $("#uploadFile4").click();
                    $("#uploadFile4").change(function(event) {
                        readURL4(this);
                        $("#uploadTrigger4").html($("#uploadFile4").val());
                    });

                }

                function uploadclick3(){
                    $("#uploadFile3").click();
                    $("#uploadFile3").change(function(event) {
                        readURL3(this);
                        $("#uploadTrigger3").html($("#uploadFile3").val());
                    });

                }
            </script>

            <script>
                function autocomplete(inp, arr) {
                    /*the autocomplete function takes two arguments,
                    the text field element and an array of possible autocompleted values:*/
                    var currentFocus;
                    /*execute a function when someone writes in the text field:*/
                    inp.addEventListener("input", function(e) {
                        var a, b, i, val = this.value;
                        /*close any already open lists of autocompleted values*/
                        closeAllLists();
                        if (!val) { return false;}
                        currentFocus = -1;
                        /*create a DIV element that will contain the items (values):*/
                        a = document.createElement("DIV");
                        a.setAttribute("id", this.id + "autocomplete-list");
                        a.setAttribute("class", "autocomplete-items");
                        /*append the DIV element as a child of the autocomplete container:*/
                        this.parentNode.appendChild(a);
                        /*for each item in the array...*/
                        for (i = 0; i < arr.length; i++) {
                            /*check if the item starts with the same letters as the text field value:*/
                            if (arr[i].substr(0, val.length).toUpperCase() == val.toUpperCase()) {
                                /*create a DIV element for each matching element:*/
                                b = document.createElement("DIV");
                                /*make the matching letters bold:*/
                                b.innerHTML = "<strong>" + arr[i].substr(0, val.length) + "</strong>";
                                b.innerHTML += arr[i].substr(val.length);
                                /*insert a input field that will hold the current array item's value:*/
                                b.innerHTML += "<input type='hidden' value='" + arr[i] + "'>";
                                /*execute a function when someone clicks on the item value (DIV element):*/
                                b.addEventListener("click", function(e) {
                                    /*insert the value for the autocomplete text field:*/
                                    inp.value = this.getElementsByTagName("input")[0].value;
                                    /*close the list of autocompleted values,
                                    (or any other open lists of autocompleted values:*/
                                    closeAllLists();
                                });
                                a.appendChild(b);
                            }
                        }
                    });
                    /*execute a function presses a key on the keyboard:*/
                    inp.addEventListener("keydown", function(e) {
                        var x = document.getElementById(this.id + "autocomplete-list");
                        if (x) x = x.getElementsByTagName("div");
                        if (e.keyCode == 40) {
                            /*If the arrow DOWN key is pressed,
                            increase the currentFocus variable:*/
                            currentFocus++;
                            /*and and make the current item more visible:*/
                            addActive(x);
                        } else if (e.keyCode == 38) { //up
                            /*If the arrow UP key is pressed,
                            decrease the currentFocus variable:*/
                            currentFocus--;
                            /*and and make the current item more visible:*/
                            addActive(x);
                        } else if (e.keyCode == 13) {
                            /*If the ENTER key is pressed, prevent the form from being submitted,*/
                            e.preventDefault();
                            if (currentFocus > -1) {
                                /*and simulate a click on the "active" item:*/
                                if (x) x[currentFocus].click();
                            }
                        }
                    });
                    function addActive(x) {
                        /*a function to classify an item as "active":*/
                        if (!x) return false;
                        /*start by removing the "active" class on all items:*/
                        removeActive(x);
                        if (currentFocus >= x.length) currentFocus = 0;
                        if (currentFocus < 0) currentFocus = (x.length - 1);
                        /*add class "autocomplete-active":*/
                        x[currentFocus].classList.add("autocomplete-active");
                    }
                    function removeActive(x) {
                        /*a function to remove the "active" class from all autocomplete items:*/
                        for (var i = 0; i < x.length; i++) {
                            x[i].classList.remove("autocomplete-active");
                        }
                    }
                    function closeAllLists(elmnt) {
                        /*close all autocomplete lists in the document,
                        except the one passed as an argument:*/
                        var x = document.getElementsByClassName("autocomplete-items");
                        for (var i = 0; i < x.length; i++) {
                            if (elmnt != x[i] && elmnt != inp) {
                                x[i].parentNode.removeChild(x[i]);
                            }
                        }
                    }
                    /*execute a function when someone clicks in the document:*/
                    document.addEventListener("click", function (e) {
                        closeAllLists(e.target);
                    });
                }

                /*An array containing all the country names in the world:*/
                var countries = ["Size","Color","Title","Meterial","Style"];

                /*initiate the autocomplete function on the "myInput" element, and pass along the countries array as possible autocomplete values:*/
                autocomplete(document.getElementById("myInput"), countries);
                autocomplete(document.getElementById("myInput2"), countries);
                autocomplete(document.getElementById("myInput3"), countries);
            </script>

    @endpush
