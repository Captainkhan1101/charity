@extends('layouts.admin-layouts')
@section('content')

    <div class="row">
        <div class="col-sm-12">
            <div class="float-right page-breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('home') }}">Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="{{ route('role.listing') }}">Category-Listing</a></li>
                    <li class="breadcrumb-item active">Category-Create</li>
                </ol>
            </div>
            <h5 class="page-title">Category</h5>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            @if(!empty( \Illuminate\Support\Facades\Session::get('message') ))
                @include('error-messages')
            @endif
            <div class="card m-b-30">
                <div class="card-body">

                    <h4 class="mt-0 header-title">Add Category</h4>
                    {{--                <p class="text-muted m-b-30 font-14"></p>--}}

                    <form action="{{ route('admin.category.save') }}" method="post">
                        @csrf
                        <div class="row">
                            <div class="col-lg-3">
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Name</label>
                                    <input type="text" class="form-control" name="name" required>
                                    @error('name')
                                    <div class="text-danger">{{$message}}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-lg-3">
                            </div>

                        </div>

                        <button type="submit" class="btn btn-primary pull-right">Submit</button>

                    </form>

                </div>
            </div>



        </div>
    </div> <!-- end col -->
    <!-- /Row -->
@endsection



