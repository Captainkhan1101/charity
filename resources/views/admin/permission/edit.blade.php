@extends('layouts.admin-layouts')

@section('content')

    <div class="row">
        <div class="col-sm-12">
            <div class="float-right page-breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('home') }}">Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="{{ route('permission.listing') }}">Permission-Listing</a></li>
                    <li class="breadcrumb-item active">Permission-Create</li>
                </ol>
            </div>
            <h5 class="page-title">Permission</h5>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            @if(!empty( \Illuminate\Support\Facades\Session::get('message') ))
                @include('error-messages')
            @endif
            <div class="card m-b-30">
                <div class="card-body">

                    <h4 class="mt-0 header-title">Add Permission</h4>
                    {{--                <p class="text-muted m-b-30 font-14"></p>--}}

                    <form action="{{ route('permission.store') }}" method="post">
                        @csrf
                        <input type="hidden" name="permission_id" value="{{  $Permissions->id }}">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Name</label>
                                    <input type="text" class="form-control"  name="name" value="{{ $Permissions->name }}">
                                    @error('name')
                                    <div class="text-danger">{{$message}}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Display Name</label>
                                    <input type="text" class="form-control"  name="display_name" value="{{ $Permissions->display_name }}">
                                    @error('display_name')
                                    <div class="text-danger">{{$message}}</div>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="row">

                            <div class="col-lg-12">
                                <label>Assign Roles</label>

                                <div class="form-group">
                                    <select class="mul-select" multiple="true" name="roles[]">
                                        @foreach($roles as $role)
                                            <option value="{{$role->id}}" @foreach($selectedRoles as $selectedRole) {{ $selectedRole->id == $role->id ? 'selected' : ''  }}  @endforeach>{{$role->display_name}}</option>
                                        @endforeach
                                    </select>
                                    @error('roles')
                                    <div class="text-danger">{{$message}}</div>
                                    @enderror
                                </div>
                                @error('roles')
                                <div class="text-danger">{{$message}}</div>
                                @enderror

                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group m-b-0">
                                    <label>Description</label>
                                    <textarea class="form-control" name="description" rows="5">{{ $Permissions->description }}</textarea>
                                    @error('description')
                                    <div class="text-danger">{{$message}}</div>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary pull-right">Submit</button
                    </form>

                </div>
            </div>



        </div>
    </div> <!-- end col -->
    <!-- /Row -->
@endsection
@push('select2-script')
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
    <script>
        $(document).ready(function(){
            $(".mul-select").select2({
                placeholder: "select role", //placeholder
                tags: true,
                tokenSeparators: ['/',',',';'," "]
            });
        })
    </script>
@endpush














