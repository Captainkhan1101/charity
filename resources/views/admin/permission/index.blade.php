@extends('layouts.admin-layouts')
@push('datatable-style')
    <!-- DataTables -->
    <link href="{{ asset('assets/plugins/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/plugins/datatables/buttons.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- Responsive datatable examples -->
    <link href="{{ asset('assets/plugins/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />

@endpush


@section('content')
    <div class="page-content-wrapper ">

        <div class="container-fluid">

            <div class="row">
                <div class="col-sm-12">
                    <div class="float-right page-breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ route('home') }}">Dashboard</a></li>
                            <li class="breadcrumb-item"><a href="{{ route('permission.listing') }}">Permission</a></li>
                            <li class="breadcrumb-item active">Permission-listing</li>
                        </ol>
                    </div>
                    <h5 class="page-title">Permission Listing</h5>
                </div>
            </div>
            <!-- end row -->

            <div class="row">
                <div class="col-12">
                    <!-- end Button -->
                    @if(!empty( \Illuminate\Support\Facades\Session::get('message') ))
                        @include('error-messages')
                    @endif
                    <div class="row">
                        <div class="col-12">
                            <a class="btn btn-info mb-3 pull-right" href="{{ route('create.permission') }}" role="button">Create New Permission</a>
                        </div>
                    </div>
                    <!-- end Button -->
                    <div class="card m-b-30">
                        <div class="card-body">

                            <h4 class="mt-0 header-title">Role Listing</h4>
                            <table id="datatable" class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                <thead>
                                <tr>
                                    <th>Sr</th>
                                    <th>Permission</th>
                                    <th>Roles</th>
                                    <th>Action</th>
                                </tr>
                                </thead>


                                <tbody>
                                @foreach($permissions as $key => $permission)
                                    <tr>
                                        <td>{{ $key+1 }}</td>
                                        <td>{{ $permission->display_name }}</td>
                                        <td>
                                            @foreach(explode(',',$permission->PermissionRoles) as $permissionRoles)
                                              <h5>
                                                <span class="badge badge-success">{{$permissionRoles}}</span>
                                              </h5>
                                            @endforeach
                                        </td>
                                        <td>
                                            <a  href="{{ route('permission.edit',  ['id' => $permission->id]) }}" ><i class="ion-edit mr-1"></i></a>
                                            <a href="{{ route('permission.delete',  ['id' => $permission->id]) }}" ><i class="ion-trash-b" style="color: red;"></i></a>

                                        </td>
                                    </tr>
                                @endforeach


                                </tbody>
                            </table>

                        </div>
                    </div>
                </div> <!-- end col -->
            </div> <!-- end row -->



        </div><!-- container fluid -->

    </div> <!-- Page content Wrapper -->
@endsection

@push('datatable-script')
    <!-- Required datatable js -->
    <script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <!-- Buttons examples -->
    <script src="{{ asset('assets/plugins/datatables/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/buttons.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/jszip.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/pdfmake.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/vfs_fonts.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/buttons.print.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/buttons.colVis.min.js') }}"></script>
    <!-- Responsive examples -->
    <script src="{{ asset('assets/plugins/datatables/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/responsive.bootstrap4.min.js') }}"></script>

    <!-- Datatable init js -->
    <script src="{{ asset('assets/pages/datatables.init.js') }}"></script>
@endpush
