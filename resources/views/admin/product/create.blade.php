@extends('layouts.admin-layouts')
@push('styles')
    <!-- Dropzone css -->
    <link href="{{asset('assets/plugins/dropzone/dist/dropzone.css')}}" rel="stylesheet" type="text/css">
    <!-- DatePicker css -->
    <link href="{{asset('assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css')}}" rel="stylesheet">
    <link href="{{ asset('assets/css/bootstrap-colorpicker.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/image-css/style.css') }}" rel="stylesheet">
    {{--    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.5/css/bootstrap.min.css">--}}
    {{--    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha512-MoRNloxbStBcD8z3M/2BmnT+rg4IsMxPkXaGh2zD6LGNNFE80W3onsAhRcMAMrSoyWL9xD7Ert0men7vR8LUZg==" crossorigin="anonymous" />--}}
    <link href="https://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootswatch/4.3.1/flatly/bootstrap.min.css">
    <link rel="stylesheet" href="{{ asset('assets/css/tagsinput.css') }}">

    <style>
        .badge { margin: 2px 5px; }
    </style>
    //atuo complete jquery

    <style>
        * {
            box-sizing: border-box;
        }

        /*body {*/
        /*    font: 16px Arial;*/
        /*}*/

        /*the container must be positioned relative:*/
        .autocomplete {
            position: relative;
            display: inline-block;
        }

        /*input {*/
        /*    border: 1px solid transparent;*/
        /*    background-color: #f1f1f1;*/
        /*    padding: 10px;*/
        /*    font-size: 16px;*/
        /*}*/

        /*input[type=text] {*/
        /*    background-color: #f1f1f1;*/
        /*    width: 100%;*/
        /*}*/

        /*input[type=submit] {*/
        /*    background-color: DodgerBlue;*/
        /*    color: #fff;*/
        /*    cursor: pointer;*/
        /*}*/

        .autocomplete-items {
            position: absolute;
            border: 1px solid #d4d4d4;
            border-bottom: none;
            border-top: none;
            z-index: 99;
            /*position the autocomplete items to be the same width as the container:*/
            top: 100%;
            left: 0;
            right: 0;
        }

        .autocomplete-items div {
            padding: 10px;
            cursor: pointer;
            background-color: #fff;
            border-bottom: 1px solid #d4d4d4;
        }

        /*when hovering an item:*/
        .autocomplete-items div:hover {
            background-color: #e9e9e9;
        }

        /*when navigating through the items using the arrow keys:*/
        .autocomplete-active {
            background-color: DodgerBlue !important;
            color: #ffffff;
        }
    </style>



@endpush
@section('content')
    <div class="page-content-wrapper ">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">



                    <div class="float-right page-breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ route('home') }}">Dashboard</a></li>
                            <li class="breadcrumb-item"><a href="{{route('admin.product.listing')}}">Products</a></li>
                            <li class="breadcrumb-item active">Create</li>
                        </ol>
                    </div>
                    <h5 class="page-title">Add Product</h5>
                </>
            </div>
            <!-- end row -->
            <div class="container" id="add-product-form" action="javascript:void(0)">
                @if(!empty( \Illuminate\Support\Facades\Session::get('message') ) || $errors->any())
                    @include('error-messages')
                @endif
                <form  action="{{ route('admin.product.store') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="row">
                        <div class="col-md-8">
                            <div class="card m-b-30">
                                <div class="card-body">
                                    <div class="form-group">
                                        <label>Title</label>
                                        <input type="text" name="title" class="form-control"  value="{{ old('title') }}" placeholder="Laptop 10th Generation" required />
                                    </div>
                                    <div class="form-group">
                                        <label>Description</label>
                                        <textarea id="elm1" name="description" value="{{ old('description') }}"  placeholder="Type something..."></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="card m-b-30">
                                <div class="card-body">
                                    <h4 class="mt-0 header-title mb-4">Media</h4>

                                    <div class="form-group">
                                        <label class="control-label col-sm-4 col-lg-12" for="edit_current_photo">Current Featured Image*</label>
                                        <div class="col-sm-6 col-lg-12">
                                            <img id="adminimg3" src="" alt="" style="width: 400px; height: 300px;">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-4 col-lg-12" for="edit_profile_photo">Select Image</label>
                                        <div class="col-sm-6 col-lg-12">
                                            <input type="file" id="uploadFile3" class="hidden" style="visibility: hidden;" name="photo" value="">
                                            <button type="button" id="uploadTrigger3" onclick="uploadclick3()" class="form-control"><i class="fa fa-download"></i> Choose Image</button>
                                            <p class="text-center">Prefered Size: (600x600) or Square Sized Image</p>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-4 col-lg-12" for="profile_photo">Product Gallery Images *<span></span></label>
                                        <div class="col-sm-6  col-lg-12">
                                            <input style="display: none;" type="file" accept="image/*" id="uploadgallery3" name="gallery[]" multiple/>
                                            <div class="margin-top">
                                                <a href="" class="btn btn-primary" data-toggle="modal" data-target="#myModal3"><i class="fa fa-plus"></i> Set Gallery</a>
                                            </div>
                                        </div>
                                    </div>


                                    <div id="myModal3" class="modal fade gallery" role="dialog">
                                        <div class="modal-dialog">

                                            <!-- Modal content-->
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h4 class="modal-title text-center">Image Gallery</h4>
                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>

                                                </div>
                                                <div class="modal-body">
                                                    <div class="gallery-btn-area text-center">
                                                        <a style="cursor: pointer;" class="btn btn-info gallery-btn mr-5" id="prod_gallery3"><i class="fa fa-download"></i> Upload Images</a>
                                                        <a style="cursor: pointer; background: #009432;" class="btn btn-info gallery-btn mr-5" data-dismiss="modal"><i class="fa fa-check" ></i> Done</a>
                                                        <p style="font-size: 11px;">You can upload multiple images.</p>
                                                    </div>

                                                    <div class="gallery-wrap" id="gallery-wrap3">
                                                        <div class="row">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card m-b-30">
                                <div class="card-body">
                                    <h4 class="mt-0 header-title mb-4">Pricing</h4>
                                    <div class="form-group">
                                        <label>Price</label>
                                        <div class="input-group">
                                            <span class="input-group-addon">Rs</span>
                                            <input data-parsley-type="number" name="price" id="product-price" value="{{ old('price') }}" oninput="calculatePrice()" type="number" class="form-control hide-number-arrow"  placeholder="0.00"required/>
                                        </div>
                                    </div>
                                    <hr class="hr-bottom">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Cost Per Item</label>
                                                <div class="input-group">
                                                    <span class="input-group-addon">Rs</span>
                                                    <input data-parsley-type="number" name="retail_price" id="cost-per-item" value="{{ old('retail_price') }}" oninput="calculatePrice()" type="number" class="form-control hide-number-arrow"  placeholder="0.00"required/>
                                                </div>
                                                <span class="font-13 text-muted">Customers won’t see this</span>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <h6>Margin</h6>
                                                    <input type="hidden" name="margin">
                                                    <p id="calculated-margin">-</p>
                                                </div>
                                                <div class="col-md-6">
                                                    <h6>Profit</h6>
                                                    <input type="hidden" name="profit">
                                                    <p id="calculated-profit">-</p>
                                                </div>
                                            </div>
                                        </div>
                                        {{--                                <div class="custom-control custom-checkbox ml-3">--}}
                                        {{--                                    <input type="checkbox" name="tax" class="custom-control-input">--}}
                                        {{--                                    <label class="custom-control-label" for="tax">Charge tax on this product</label>--}}
                                        {{--                                </div>--}}
                                    </div>
                                </div>
                            </div>
                            <div class="card m-b-30">
                                <div class="card-body">
                                    <h4 class="mt-0 header-title mb-4">Inventory</h4>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">

                                                <label>SKU (Stock Keeping Unit)</label>
                                                <input name="sku" id="product-price" oninput="calculatePrice()" type="text" class="form-control" value="{{old('sku')}}"  placeholder="0.00"required/>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Barcode (ISBN, UPC, GTIN, etc.)</label>
                                                <input id="product-price" name="barcode" oninput="calculatePrice()" type="text" class="form-control" value="{{old('barcode')}}" placeholder="0.00"required/>
                                            </div>
                                        </div>
                                    </div>
                                    {{--                            <div class="custom-control custom-checkbox">--}}
                                    {{--                                <input type="checkbox" class="custom-control-input" name="track-quantity">--}}
                                    {{--                                <label class="custom-control-label" for="track-quantity">Track Quantity</label>--}}
                                    {{--                            </div>--}}
                                    {{--                            <div class="custom-control custom-checkbox">--}}
                                    {{--                                <input type="checkbox" class="custom-control-input" name="continue-selling">--}}
                                    {{--                                <label class="custom-control-label" for="continue-selling">Continue selling when out of stock</label>--}}
                                    {{--                            </div>--}}
                                    <hr class="hr-bottom">
                                    <h4 class="mt-0 header-title mb-4">QUANTITY</h4>
                                    <div class="form-group">
                                        <label>Available</label>
                                        <input type="number" name="quantity" class="form-control"  value="{{ old('quantity') }}" placeholder="0"required/>
                                    </div>
                                </div>
                            </div>
                            <div class="card m-b-30" >
                                <div class="card-body">
                                    <h4 class="mt-0 header-title ">Shipping</h4>
                                    <hr class="hr-bottom">
                                    <h4 class="mt-0 header-title "><strong>WEIGHT</strong></h4>
                                    <p class="text-muted  font-14">
                                        Used to calculate shipping rates at checkout and label prices during fulfillment.
                                    </p>
                                    <div class="row">
                                        <div class="col-6">
                                    <h5 class="mt-0 header-title">Weight</h5>
                                    <input type="number" name="weight" class="form-control"  value="{{ old('weight') }}" placeholder="0"required/>
                                        </div>
                                        <div class="col-2">
                                            <h5 class="mt-0 header-title">Unit</h5>
                                            <select class="form-control" name="unit">

                                                <option value="kg">kg</option>
                                                <option value="g" >g</option>
                                                <option value="lb">lb</option>
                                                <option value="oz" >oz</option>

                                            </select>
                                        </div>
                                        </div>
                                    <div class="row mt-3">
                                        <div class="col-6">
                                            <h5 class="mt-0 header-title">Width</h5>
                                            <input type="number" name="width" class="form-control"  value="{{ old('width') }}" placeholder="0"required/>
                                        </div>
                                        <div class="col-6">
                                            <h5 class="mt-0 header-title">Height</h5>
                                            <input type="number" name="height" class="form-control"  value="{{ old('height') }}" placeholder="0"required/>

                                        </div>
                                    </div>
                                    <div class="custom-control custom-checkbox">

                                    </div>
                                </div>
                            </div>
                            <div class="card m-b-30">
                                <div class="card-body">
                                    <h4 class="mt-0 header-title mb-4">Variants</h4>
                                    <div class="custom-control custom-checkbox">
                                        <input onchange="showOrHideMultiVariants()" id="multiple-variant" type="checkbox" class="custom-control-input" name="multiple-variant">
                                        <label class="custom-control-label" for="multiple-variant">
                                            This product has multiple options, like different sizes or colors
                                        </label>
                                    </div>
                                    <div id="product-multi-variant-container" style="display:none;">
                                        <hr class="hr-bottom">
                                        <h4 class="mt-0 header-title mb-2">OPTIONS</h4>
                                        <div class='element' id='div_1'>

                                            <div id="product-multi-variant-options" class="product-element">
                                                <h4 class="mt-0 header-title mb-2"><strong>Option1</strong></h4>
                                                <div class="row">
                                                    <div class="col-3">
                                                        <div class="autocomplete" >
                                                            <input id="myInput" type="text" class="form-control variant-inp" name="variant[]" placeholder="Size" >
                                                        </div>
                                                    </div>
                                                    <div class="col-8">
                                                        <input type="text" value="" name="variant_add[0]" onchange="product_variants()" class="variant-add-inp" data-role="tagsinput" />
                                                    </div>

                                                </div>
                                                <hr>
                                            </div>

                                        </div>
                                        &nbsp;<button class="add" type="button">Add More option</button>






                                        {{--                                <div class="form-group">--}}
                                        {{--                                    <label class="control-label col-sm-4" for="email"></label>--}}
                                        {{--                                    <div class="col-sm-6">--}}
                                        {{--                                        <div class="checkbox2">--}}
                                        {{--                                            <input type="checkbox" id="check3" name="colcheck" value="1">--}}
                                        {{--                                            <label for="check3">Allow Product Colors</label>--}}
                                        {{--                                        </div>--}}
                                        {{--                                    </div>--}}
                                        {{--                                </div>--}}

                                        <div id="fimg1" style="display: none;">
                                            <div class="color-area" id="q1">
                                                <div class="form-group  single-color">
                                                    <label class="control-label col-sm-4" for="blood_group_display_name">Product Colors* <span>(Choose Your Favourite Color.)</span></label>
                                                    <div class="col-sm-6">
                                                        <div class="input-group colorpicker-component">
                                                            <input type="text" name="color[]" value="#000000" onclick="product_variants();"  class="form-control colorpick" />
                                                            <span class="input-group-addon"><i></i></span>
                                                            <span class="ui-close1" id="parentclose" onclick="product_variants();">X</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <br>
                                            <div class="form-group">
                                                <div class="col-sm-5 col-sm-offset-4">
                                                    <button class="btn btn-default featured-btn" type="button" id="add-color"><i class="fa fa-plus"></i> Add More Color</button>
                                                </div>
                                            </div>
                                        </div>

                                        <div id="product-multi-variant-preview" style="overflow-x:auto; margin-top: 10px;">
                                            <h5>PREVIEW</h5>
                                            <table class="table">
                                                <thead>
                                                <tr>
                                                    <th>variant</th>
                                                    <th>Price</th>
                                                    <th>Quantity</th>
                                                    <th>SKU</th>
                                                    <th>Barcode</th>
                                                    <th></th>
                                                </tr>
                                                </thead>
                                                <tbody id="preview-variant">

                                                </tbody>
                                            </table>

                                        </div>

                                    </div>
                                </div>
                                 <!-- end col -->

                               
                            </div>
                            <div class="card m-b-30">
                                <div class="card-body">

                                    <h6 class="mt-0 header-title mb-4">
                                        Search engine listing preview
                                        <i class="fa fa-edit float-right cursor-pointer" onclick="$('#publish-seo-div').toggle()">Edit website SEO</i>
                                    </h6>
                                    <h6 class="mt-0 header-title mb-4">
                                    Add a title and description to see how this product might appear in a search engine listing
                                    </h6>
                                        <div id="publish-seo-div" style="display:none;">
                                        <div class="row">
                                            <div class="col-12 mt-4">
                                                <h5 class="mt-0 header-title">Page Title</h5>
                                                <input type="text" name="seo_title" class="form-control"  value="{{ old('seo_title') }}" placeholder="Enter Title"/>
                                            </div>
                                            <div class="col-12 mt-4">
                                                <h5 class="mt-0 header-title">Description</h5>
                                                <textarea type="text" name="seo_description" class="form-control"  value="{{ old('seo_description') }}" />write description ...
                                                </textarea>

                                            </div>
                                            <div class="col-12 mt-4">
                                                <h5 class="mt-0 header-title">URL and handle</h5>
                                                <textarea type="text" name="seo_url" class="form-control"  value="{{ old('seo_url') }}" placeholder="https://www.system.com/">
                                                </textarea>

                                            </div>
                                        </div>


                                    </div>
                                </div>
                                <!-- end col -->


                            </div>
                        </div><!-- end col -->
                        <div class="col-md-4">
                            <div class="card m-b-30">

                                <div class="card-body pb-1">
                                    <h4 class="mt-0 header-title mb-4">Product availability</h4>
                                    <p class="text-muted m-b-30 font-14">
                                        Available on 0 of 1 channels and apps
                                    </p>
                                    <hr class="hr-bottom">
                                    <h6 class="mt-0 header-title mb-4">
                                        Online Store
                                        <i class="fa fa-calendar float-right cursor-pointer" onclick="$('#publish-date-div').toggle()"></i>
                                    </h6>
                                    <div id="publish-date-div" style="display: none">
                                        <p class="text-muted m-b-30 font-14">Publish product on</p>
                                        <div class="row">
                                            <div class="form-group col-md-5 pr-0">
                                                <div class="input-group">
                                                    <input type="text" name="product_availability_date" class="form-control datepicker-auto-close" placeholder="Date" >
                                                </div><!-- input-group -->
                                            </div>
                                            {{--                                        <div class="form-group col-md-6 pl-1">--}}
                                            {{--                                            <div class="input-group">--}}
                                            {{--                                                <input type="time" class="form-control border-right-0" placeholder="Time">--}}
                                            {{--                                                <div class="input-group-append bg-custom b-0"><span class="input-group-text input-group-addon">GMT+5</span></div>--}}
                                            {{--                                            </div><!-- input-group -->--}}
                                            {{--                                        </div>--}}
                                            <div class="form-group col-md-1 pl-0 pt-1" onclick="refreshAvailableForm()">
                                                <i class="fa fa-close cursor-pointer"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="card m-b-30 grey-bg-color">
                                <div class="card-body pb-1">
                                    <h4 class="mt-0 header-title mb-4">Organization</h4>
                                    <div class="form-group">
                                        <label>Collection</label>
                                        <select name="category_id[]" class="form-control" id="category-mu"  multiple>
                                            <option value="">Please select an option</option>
                                            @foreach($categories as $category)
                                                <option value="{{ $category->id }}" >{{ $category->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
{{--                                    <div class="form-group">--}}
{{--                                        <label>Sub category</label>--}}
{{--                                        <select name="subcategory_id" class="form-control">--}}
{{--                                            <option value="" selected>Please select an option</option>--}}
{{--                                            @foreach($subCategories as $subCategory)--}}
{{--                                                <option value="{{ $subCategory->id }}">{{ $subCategory->sub_name }}</option>--}}
{{--                                            @endforeach--}}

{{--                                        </select>--}}
{{--                                    </div>--}}
{{--                                    <div class="form-group">--}}
{{--                                        <label>Child category</label>--}}
{{--                                        <select name="childcategory_id" class="form-control" >--}}
{{--                                            <option value="" selected>Plese select an option</option>--}}
{{--                                            @foreach($childCategories as $childCategory)--}}
{{--                                                <option value="{{ $childCategory->id }}">{{ $childCategory->child_name }}</option>--}}
{{--                                            @endforeach--}}

{{--                                        </select>--}}
{{--                                    </div>--}}
                                    {{--                            <div class="form-group">--}}
                                    {{--                                <label>Vendor</label>--}}
                                    {{--                                <input type="number" class="form-control"  placeholder="e.g. Dell"/>--}}
                                    {{--                            </div>--}}
                                </div>
                            </div>
                        </div> <!-- end col -->
                    </div> <!-- end row -->
                                        <button type="reset" class="btn btn-secondary waves-effect m-l-5 pull-right ml-2 mr-3">
                                            Cancel
                                        </button>
                                        <button type="submit" class="btn btn-primary waves-effect waves-light pull-right" id="pro-sub">
                                            Submit
                                        </button>
                </form>
            </div>
        </div>
    </div>
@endsection
@push('scripts')


    <script src="{{ asset('assets/js/tagsinput.js') }}"></script>
    {{--    <script src="{{ asset('assets/js/bootstrap-tagsinput-angular.min.js') }}" ></script>--}}
    <!-- Parsley js -->
    <script src="{{asset('assets/plugins/parsleyjs/parsley.min.js')}}"></script>
    <!--Wysiwig js-->
    <script src="{{asset('assets/plugins/tinymce/tinymce.min.js')}}"></script>
    <!-- Dropzone js -->
    <script src="{{asset('assets/plugins/dropzone/dist/dropzone.js')}}"></script>
    <!-- DatePicker js -->
    <script src="{{asset('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"></script>
    <script>
        $(document).ready(function() {
            $('#add-product-form').parsley();
        });
    </script>
    <script src="{{  asset('assets/js/bootstrap-colorpicker.js') }}"></script>
    <script src="{{  asset('assets/js/bootstrap-colorpicker.min.js') }}"></script>

    <!--
        ---------- Custom Js ----------
    -->
    <script src="{{  asset('assets/js/gallery.js') }}"></script>
    <script src="{{  asset('assets/js/variants.js') }}"></script>
    <script src="{{  asset('assets/js/autocomplete.js') }}"></script>
    <!--
        --------- End of Custom Js ----------
    -->

    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-42755476-1', 'bootstrap-tagsinput.github.io');
        ga('send', 'pageview');
    </script>
    <script>

        $('#category-mu').multiSelect();
    </script>
@endpush
