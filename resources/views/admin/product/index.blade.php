@extends('layouts.admin-layouts')
@push('datatable-style')
    <!-- DataTables -->
    <link href="{{ asset('assets/plugins/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/plugins/datatables/buttons.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- Responsive datatable examples -->
    <link href="{{ asset('assets/plugins/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />

@endpush
@section('content')
<div class="page-content-wrapper ">

    <div class="container-fluid">

        <div class="row">
            <div class="col-sm-12">
                <div class="float-right page-breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{  route('home') }}">Dashoard</a></li>
                        <li class="breadcrumb-item"><a href="{{  route('admin.product.listing') }}">Product</a></li>
                        <li class="breadcrumb-item active">Product-listing</li>
                    </ol>
                </div>
                <h5 class="page-title">Products</h5>
            </div>
        </div>
        <!-- end row -->

        <div class="row mb-2">
            <div class="col-12">
                <button class="btn btn-primary float-right" onclick="location.href='{{route('admin.product.create')}}'"> Add Product</button>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                @if(!empty( \Illuminate\Support\Facades\Session::get('message') ))
                    @include('error-messages')
                @endif
                <div class="card m-b-30">
                    <div class="card-body">
                        <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                            <thead>
                            <tr>
                                <th>Sr</th>
                                <th>Title</th>
                                <th>Description</th>
                                <th>Price</th>
                                <th>Image</th>
                                <th>Sku</th>
                                <th>Bar Code</th>
                                <th>Quanity</th>
                                <th>Availiablity Date</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($products as $key => $product)
                                <tr>
                                    <td>{{ $key+1 }}</td>
                                    <td>{{ $product['title'] }}</td>
                                    <td>{{ $product['description'] }}</td>
                                    <td>{{ $product['price'] }}</td>
                                    <td> <img src="{{ asset($product['photo']) }}" alt="..." style="width: 50px; height: 50px;"></td>
                                    <td>{{ $product['sku'] }}</td>
                                    <td>{{ $product['barcode'] }}</td>
                                    <td>{{ $product['quantity'] }}</td>
                                    <td>{{ $product['product_availability_date'] }}</td>
                                    <td>
                                        <a href="{{ route('admin.product.edit', ['id' => $product['id'] ]) }}" class="mr-1"><i class="ion-edit"></i></a>
                                        <a href="{{ route('admin.product.delete', ['id' => $product['id'] ]) }}" onclick="return confirm('Are you sure you want to delete this item')"><i class="ion-trash-b" style="color: red;"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div> <!-- end col -->
        </div> <!-- end row -->
    </div>
</div>
@endsection
@push('datatable-script')
    <!-- Required datatable js -->
    <script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <!-- Buttons examples -->
    <script src="{{ asset('assets/plugins/datatables/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/buttons.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/jszip.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/pdfmake.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/vfs_fonts.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/buttons.print.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/buttons.colVis.min.js') }}"></script>
    <!-- Responsive examples -->
    <script src="{{ asset('assets/plugins/datatables/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/responsive.bootstrap4.min.js') }}"></script>

    <!-- Datatable init js -->
    <script src="{{ asset('assets/pages/datatables.init.js') }}"></script>
@endpush
