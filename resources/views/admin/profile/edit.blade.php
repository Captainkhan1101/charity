@extends('layouts.admin-layouts')
@section('content')

    <div class="row">
         <div class="col-xl-12">
         @if(!empty( \Illuminate\Support\Facades\Session::get('message') ))
                @include('error-messages')
            @endif
                                    <div class="card m-b-30">
                                        <div class="card-body">
            
                                            <h4 class="mt-0 header-title">Account Settings</h4>
                                            
            
                                            <!-- Nav tabs -->
                                            <ul class="nav nav-tabs" role="tablist">
                                                
                                                <li class="nav-item">
                                                    <a class="nav-link active" data-toggle="tab" href="#profile" role="tab">Profile Info</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" data-toggle="tab" href="#password-reset" role="tab">Password Reset</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" data-toggle="tab" href="#settings" role="tab">Bank Detail</a>
                                                </li>
                                            </ul>
            
                                            <!-- Tab panes -->
                                            <div class="tab-content">
                                                
                                                <div class="tab-pane p-3 active" id="profile" role="tabpanel">
                                                    <p class="font-14 mb-0">
                                                      <form action="{{ route('admin.user.save') }}" method="post">
                        @csrf
                        <input type="hidden" value="{{ $user->id  }}" name="id">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Username</label>
                                    <input type="text" class="form-control" name="name" placeholder="Enter user name" value="{{ $user->name }}" required>
                                    @error('name')
                                    <div class="text-danger">{{$message}}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Email</label>
                                    <input type="email" class="form-control" name="email" value="{{ $user->email }}" placeholder="Enter email address" required>
                                    @error('display_name')
                                    <div class="text-danger">{{$message}}</div>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Assign Role</label>
                                    <select name="role_id" class="form-control" required>
                                        <option value="">Please select an option</option>
                                        @if(!empty($roles))
                                            @foreach($roles as $role)
                                                <option value="{{ $role->id }}" {{ $role->id == $user->roleUser->role_id ? 'selected' : ''  }}> {{ $role->name }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                    @error('display_name')
                                    <div class="text-danger">{{$message}}</div>
                                    @enderror
                                </div>
                            </div>

                        </div>

                        <button type="submit" class="btn btn-primary pull-right">Submit</button>
                    </form>
                                                    </p>
                                                </div>
                                                <div class="tab-pane p-3" id="password-reset" role="tabpanel">
                                                     <form action="{{ route('update.reset.password') }}" method="POST">
                                    {{ csrf_field() }}
                                    <div class="form-body">
                                       
                                        <!--/name-->
                                        <div class="row">
                                            <div class="col-md-3">
                                               
                                            </div>
                                            <!--/span-->
                                            <div class="col-md-6">
                                                <div class="form-group{{ $errors->has('old_password') ? ' has-danger' : '' }}">
                                                    <label class="control-label">Old Password</label>
                                                    <input type="password" id="lastName" class="form-control " name="old_password" placeholder="Enter Old Password" >
                                                      @if ($errors->has('old_password'))
                                                <small class="form-control-feedback"> {{ $errors->first('old_password') }} </small>
                                    
                                @endif</div>
                                            </div>
                                             <div class="col-md-3">
                                               
                                            </div>
                                            <!--/span-->
                                        </div>
                                         <div class="row">
                                            <div class="col-md-3">
                                               
                                            </div>
                                            <!--/span-->
                                            <div class="col-md-6">
                                                <div class="form-group{{ $errors->has('password') ? ' has-danger' : '' }} ">
                                                    <label class="control-label">New Password</label>
                                                    <input type="password" id="myPassword" name="password" class="form-control" >
                                                   @if ($errors->has('password'))
                                                <small class="form-control-feedback"> {{ $errors->first('password') }} </small>
                                    
                                @endif  <div id="errors-Password" style="color:red;" ></div></div>
                                            </div>
                                             <div class="col-md-3">
                                               
                                            </div>
                                            <!--/span-->
                                        </div>
                                        <!--/rowDOB/gender-->
                                         <div class="row">
                                            <div class="col-md-3">
                                               
                                            </div>
                                            <!--/span-->
                                            <div class="col-md-6">
                                                <div class="form-group{{ $errors->has('password_confirmation') ? ' has-danger' : '' }}">
                                                    <label class="control-label">Confirm Password</label>
                                                    <input type="password" id="myConfirmPassword" name="password_confirmation" class="form-control" >
                                                      @if ($errors->has('password_confirmation'))
                                                <small class="form-control-feedback"> {{ $errors->first('password_confirmation') }} </small>
                                              
                                @endif
                                  </div>
                                            </div>
                                             <div class="col-md-3">
                                               
                                            </div>
                                            <!--/span-->
                                        </div>
                                        <!--/row--Contact-->
                                      
                                        <!--/row-->
                                        <!--/row-->
                                    </div>
                                     <div class="row">
                                            <div class="col-md-3">
                                               
                                            </div>
                                            <!--/span-->
                                            <div class="col-md-6">
                                               <button type="submit" class="btn btn-primary align-self-center" style="width: 100%;">
                                    Reset Password
                                </button>
                                            </div>
                                             <div class="col-md-3">
                                               
                                            </div>
                                            <!--/span-->
                                        </div>
                                  
                                </form>
                                                </div>
                                                <div class="tab-pane p-3" id="settings" role="tabpanel">
                                                    <p class="font-14 mb-0">
                                                        
                                                    </p>
                                                </div>
                                            </div>
            
                                        </div>
                                    </div>
                                </div>
    </div> <!-- end col -->
    <!-- /Row -->
@endsection
@push('scripts')

<script src="{{ asset('assets/js/jquery.password-validation.js') }}"></script>

<script>
	$(document).ready(function() {
		$("#myPassword").passwordValidation({"confirmField": "#myConfirmPassword"}, function(element, valid, match, failedCases) {

        
            
		    $("#errors-Password").html('<pre style="color:red;">' + failedCases.join("\n") + '</pre>');
		  
		     if(valid) $(element).css("border","2px solid green");
		     if(!valid) $(element).css("border","2px solid red");
		     if(valid && match) $("#myConfirmPassword").css("border","2px solid green");
		     if(!valid || !match) $("#myConfirmPassword").css("border","2px solid red");
		});
	});
</script>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-36251023-1']);
  _gaq.push(['_setDomainName', 'jqueryscript.net']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>



@endpush
