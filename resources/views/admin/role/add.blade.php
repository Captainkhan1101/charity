@extends('layouts.admin-layouts')
@section('content')

        <div class="row">
            <div class="col-sm-12">
                <div class="float-right page-breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('home') }}">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('role.listing') }}">Role-Listing</a></li>
                        <li class="breadcrumb-item active">Role-Create</li>
                    </ol>
                </div>
                <h5 class="page-title">Role</h5>
            </div>
        </div>
    <div class="row">
        <div class="col-lg-12">
            @if(!empty( \Illuminate\Support\Facades\Session::get('message') ))
                @include('error-messages')
            @endif
            <div class="card m-b-30">
                <div class="card-body">

                    <h4 class="mt-0 header-title">Add Role</h4>
                    {{--                <p class="text-muted m-b-30 font-14"></p>--}}

                    <form action="{{ route('role.store') }}" method="post">
                        @csrf
                        <div class="row">
                            <div class="col-lg-6">
                        <div class="form-group">
                            <label>Name</label>
                            <input type="text" class="form-control" name="name">
                            @error('name')
                            <div class="text-danger">{{$message}}</div>
                            @enderror
                        </div>
                            </div>
                            <div class="col-lg-6">
                        <div class="form-group">
                            <label>Display Name</label>
                            <input type="text" class="form-control" name="display_name">
                            @error('display_name')
                            <div class="text-danger">{{$message}}</div>
                            @enderror
                        </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                        <div class="form-group m-b-0">
                            <label>Description</label>
                            <textarea class="form-control" name="description" rows="5"></textarea>
                            @error('description')
                            <div class="text-danger">{{$message}}</div>
                            @enderror
                        </div>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary pull-right">Submit</button
                    </form>

                </div>
            </div>



        </div>
    </div> <!-- end col -->
<!-- /Row -->
@endsection



