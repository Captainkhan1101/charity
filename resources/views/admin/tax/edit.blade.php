@extends('layouts.admin-layouts')
@section('content')

    <div class="row">
        <div class="col-sm-12">
            <div class="float-right page-breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('home') }}">Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="{{ route('role.listing') }}">Child Category-Listing</a></li>
                    <li class="breadcrumb-item active">Child-Category-Create</li>
                </ol>
            </div>
            <h5 class="page-title">Child Category</h5>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            @if(!empty( \Illuminate\Support\Facades\Session::get('message') ))
                @include('error-messages')
            @endif
            <div class="card m-b-30">
                <div class="card-body">

                    <h4 class="mt-0 header-title">Add Child Category</h4>
                    {{--                <p class="text-muted m-b-30 font-14"></p>--}}

                    <form action="{{ route('admin.tax.save') }}" method="post">
                        @csrf
                        <input type="hidden" name="id" value="{{ $tax->id }}">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Tax</label>
                                    <input type="text" class="form-control" name="tax" value="{{ $tax->tax }}" required>
                                    @error('name')
                                    <div class="text-danger">{{$message}}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>States</label>
                                    <select name="us_state_id"  class="form-control" required>
                                        <option value="" selected >Select an option</option>
                                        @foreach($UsStates as $UsState)
                                            <option value="{{  $UsState->id }}" {{ $UsState->id == $tax->us_state_id ? 'selected' : '' }}>{{  $UsState->name }}</option>
                                        @endforeach

                                    </select>
                                </div>
                            </div>

                        </div>

                        <button type="submit" class="btn btn-primary pull-right">Submit</button>

                    </form>

                </div>
            </div>



        </div>
    </div> <!-- end col -->
    <!-- /Row -->
@endsection



