@extends('layouts.admin-layouts')
@push('datatable-style')
    <!-- DataTables -->
    <link href="{{ asset('assets/plugins/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/plugins/datatables/buttons.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- Responsive datatable examples -->
    <link href="{{ asset('assets/plugins/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />

@endpush
@section('content')
    <div class="page-content-wrapper ">

        <div class="container-fluid">

            <div class="row">
                <div class="col-sm-12">
                    <div class="float-right page-breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{  route('home') }}">Dashoard</a></li>
                            <li class="breadcrumb-item"><a href="{{ route('admin.childcategory.listing') }}">Tax</a></li>
                            <li class="breadcrumb-item active">Tax Listing</li>
                        </ol>
                    </div>
                    <h5 class="page-title">Tax Listing</h5>
                </div>
            </div>
            <!-- end row -->

            <div class="row">
                <div class="col-12">
                @if(!empty( \Illuminate\Support\Facades\Session::get('message') ))
                    @include('error-messages')
                @endif
                <!-- end Button -->
                    <div class="row">
                        <div class="col-12">
                            <a class="btn btn-info mb-3 pull-right" href="{{ route('admin.tax.create') }}" role="button">Create Tax</a>
                        </div>
                    </div>
                    <!-- end Button -->
                    <div class="card m-b-30">
                        <div class="card-body">

                            <h4 class="mt-0 header-title">Tax Listing</h4>
                            <table id="datatable" class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                <thead>
                                <tr>
                                    <th>Sr</th>
                                    <th>Tax</th>
                                    <th>State</th>
                                    <th>Action</th>
                                </tr>
                                </thead>


                                <tbody>
                                @if(!empty($taxes))
                                    @foreach($taxes as $key => $tax)
                                        <tr>
                                            <td>{{ $key+1 }}</td>
                                            <td>{{ $tax->tax }}</td>
                                            <td>{{ !empty($tax->UsState)? $tax->UsState->name : '' }}</td>
                                            <td>



                                                <a  href="{{ route('admin.tax.edit', ['id' => $tax->id])  }}"><i class="ion-edit mr-1" ></i></a>
                                                <a  href="{{ route('admin.tax.delete', ['id' => $tax->id])  }}"   onclick="return confirm(' you want to delete?');" ><i class="ion-trash-b" style="color:red;"></i></a>

                                            </td>
                                        </tr>
                                    @endforeach
                                @endif


                                </tbody>
                            </table>

                        </div>
                    </div>
                </div> <!-- end col -->
            </div> <!-- end row -->



        </div><!-- container fluid -->

    </div> <!-- Page content Wrapper -->
@endsection

@push('datatable-script')
    <!-- Required datatable js -->
    <script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <!-- Buttons examples -->
    <script src="{{ asset('assets/plugins/datatables/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/buttons.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/jszip.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/pdfmake.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/vfs_fonts.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/buttons.print.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/buttons.colVis.min.js') }}"></script>
    <!-- Responsive examples -->
    <script src="{{ asset('assets/plugins/datatables/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/responsive.bootstrap4.min.js') }}"></script>

    <!-- Datatable init js -->
    <script src="{{ asset('assets/pages/datatables.init.js') }}"></script>
@endpush
