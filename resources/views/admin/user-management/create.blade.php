@extends('layouts.admin-layouts')
@section('content')

    <div class="row">
        <div class="col-sm-12">
            <div class="float-right page-breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('home') }}">Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="{{ route('role.listing') }}">User-Listing</a></li>
                    <li class="breadcrumb-item active">User-Create</li>
                </ol>
            </div>
            <h5 class="page-title">User</h5>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            @if(!empty( \Illuminate\Support\Facades\Session::get('message') ))
                @include('error-messages')
            @endif
            <div class="card m-b-30">
                <div class="card-body">

                    <h4 class="mt-0 header-title">Add User</h4>
                    {{--                <p class="text-muted m-b-30 font-14"></p>--}}

                    <form action="{{ route('admin.user.save') }}" method="post">
                        @csrf
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Username</label>
                                    <input type="text" class="form-control" name="name" placeholder="Enter user name" required>
                                    @error('name')
                                    <div class="text-danger">{{$message}}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Email</label>
                                    <input type="email" class="form-control" name="email" placeholder="Enter email address" required>
                                    @error('display_name')
                                    <div class="text-danger">{{$message}}</div>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Assign Role</label>
                                    <select name="role_id" class="form-control" required>
                                        <option value="">Please select an option</option>
                                        @if(!empty($roles))
                                            @foreach($roles as $role)
                                        <option value="{{ $role->id }}"> {{ $role->name }}</option>
                                            @endforeach
                                            @endif
                                    </select>
                                    @error('display_name')
                                    <div class="text-danger">{{$message}}</div>
                                    @enderror
                                </div>
                            </div>

                        </div>

                        <button type="submit" class="btn btn-primary pull-right">Submit</button
                    </form>

                </div>
            </div>



        </div>
    </div> <!-- end col -->
    <!-- /Row -->
@endsection



