@extends('layouts.admin-layouts')
@push('datatable-style')
    <!-- DataTables -->
    <link href="{{ asset('assets/plugins/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/plugins/datatables/buttons.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- Responsive datatable examples -->
    <link href="{{ asset('assets/plugins/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />

@endpush
@section('content')
    <div class="page-content-wrapper ">

        <div class="container-fluid">

            <div class="row">
                <div class="col-sm-12">
                    <div class="float-right page-breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{  route('home') }}">Dashoard</a></li>
                            <li class="breadcrumb-item"><a href="{{ route('admin.user.listing') }}">User</a></li>
                            <li class="breadcrumb-item active">User-listing</li>
                        </ol>
                    </div>
                    <h5 class="page-title">User Listing</h5>
                </div>
            </div>
            <!-- end row -->

            <div class="row">
                <div class="col-12">
                @if(!empty( \Illuminate\Support\Facades\Session::get('message') ))
                    @include('error-messages')
                @endif
                <!-- end Button -->
                    <div class="row">
                        <div class="col-12">
                            <a class="btn btn-info mb-3 pull-right" href="{{ route('admin.user.create') }}" role="button">Create New User</a>
                        </div>
                    </div>
                    <!-- end Button -->
                    <div class="card m-b-30">
                        <div class="card-body">

                            <h4 class="mt-0 header-title">User Listing</h4>
                            <table id="datatable" class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                <thead>
                                <tr>
                                    <th>Sr</th>
                                    <th>Name</th>
                                    <th>Email</th>

                                    <th>Action</th>
                                </tr>
                                </thead>


                                <tbody>
                                @foreach($users as $key => $user)
                                    <tr>
                                        <td>{{ $key+1 }}</td>
                                        <td>{{ $user->name }}</td>
                                        <td>{{ $user->email }}</td>

                                        <td>
                                            <a  href="{{ route('admin.user.edit', ['id' => $user->id])  }}" title="Edit User Detail" ><i class="ion-edit mr-1"></i></a>
                                            <a  href="{{ route('admin.user.delete', ['id' => $user->id])  }}" title="Delete User"  onclick="return confirm('Are you sure you want to delete this item')"><i class="ion-trash-b" style="color: red;"></i></a>
                                            <a  href="{{ route('admin.user.reset.password', ['email' => $user->email])  }}" title="Reset Password Link"  onclick="return confirm('Are you sure you want to send password link?')"><i class="ion-refresh" style="color: red;"></i></a>

                                        </td>
                                    </tr>
                                @endforeach


                                </tbody>
                            </table>

                        </div>
                    </div>
                </div> <!-- end col -->
            </div> <!-- end row -->



        </div><!-- container fluid -->

    </div> <!-- Page content Wrapper -->
@endsection

@push('datatable-script')
    <!-- Required datatable js -->
    <script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <!-- Buttons examples -->
    <script src="{{ asset('assets/plugins/datatables/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/buttons.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/jszip.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/pdfmake.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/vfs_fonts.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/buttons.print.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/buttons.colVis.min.js') }}"></script>
    <!-- Responsive examples -->
    <script src="{{ asset('assets/plugins/datatables/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/responsive.bootstrap4.min.js') }}"></script>

    <!-- Datatable init js -->
    <script src="{{ asset('assets/pages/datatables.init.js') }}"></script>
@endpush
