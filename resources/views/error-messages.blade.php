@if(!empty( \Illuminate\Support\Facades\Session::get('message') ))
<div class="alert {{ \Illuminate\Support\Facades\Session::get('alert-class', 'alert-info') }}  s alert-dismissible fade show " role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <strong>{{ \Illuminate\Support\Facades\Session::get('message') }}</strong>
</div>
@endif
@if($errors->any())
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <ul>
            @foreach ($errors->all() as $error)
                <li><strong>{{ $error }}</strong></li>
            @endforeach
        </ul>
    </div>

@endif




