<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::group(['prefix'=>'admin'], function(){
Route::get('product-listing',  ['middleware' => ['permission:product-listing'], 'uses' => 'ProductController@index'])->name('admin.product.listing');
Route::get('product-create',  ['middleware' => ['permission:product-create'], 'uses' => 'ProductController@create'])->name('admin.product.create');
Route::post('product-store', ['middleware' => ['permission:product-store'], 'uses' => 'ProductController@store'])->name('admin.product.store');
Route::get('product-delete', ['middleware' => ['permission:product-delete'], 'uses' => 'ProductController@delete'])->name('admin.product.delete');
Route::get('product-edit',['middleware' => ['permission:product-edit'], 'uses' => 'ProductController@edit'])->name('admin.product.edit');

/**
 * UserController route
 **/

Route::GET('/user-listing', ['middleware' => ['permission:user-listing'], 'uses' => 'UserController@index'])->name('admin.user.listing');
Route::GET('user-create',['middleware' => ['permission:user-create'], 'uses' => 'UserController@create'])->name('admin.user.create');
Route::GET('user-edit', ['middleware' => ['permission:user-edit'], 'uses' => 'UserController@edit'])->name('admin.user.edit');
Route::GET('user-delete', ['middleware' => ['permission:user-delete'], 'uses' => 'UserController@delete'])->name('admin.user.delete');
Route::POST('user-save',['middleware' => ['permission:user-save'], 'uses' => 'UserController@store'])->name('admin.user.save');
Route::GET('user-reset-password','UserController@resentPasswordLink')->name('admin.user.reset.password');




/**
 * PermissionController route
 **/

Route::GET('/permission-listing', 'PermissionController@index')->name('permission.listing');
Route::GET('permission-create','PermissionController@create')->name('create.permission');
Route::GET('permission-edit','PermissionController@edit')->name('permission.edit');
Route::GET('permission-delete','PermissionController@delete')->name('permission.delete');
Route::POST('permission-store','PermissionController@store')->name('permission.store');


/**
 * UserStatusController route
 **/

Route::POST("/update/status", "UserStatusController@update");

/**
 * RoleController route
 **/
Route::GET('role-listing','RoleController@index')->name('role.listing');
Route::GET('role-create','RoleController@create')->name('role.create');
Route::POST('role-store','RoleController@store')->name('role.store');
Route::GET('role-edit','RoleController@edit')->name('role.edit');
Route::GET('role-delete','RoleController@delete')->name('role.delete')
;


/**
 * CategoryController route
 **/
Route::GET('category-listing', ['middleware' => ['permission:category-listing'], 'uses' => 'CategoryController@index'])->name('admin.category.listing');
Route::GET('category-create', ['middleware' => ['permission:category-create'], 'uses' => 'CategoryController@create'])->name('admin.category.create');
Route::POST('category-save',['middleware' => ['permission:category-save'], 'uses' => 'CategoryController@save'])->name('admin.category.save');
Route::GET('category-delete', ['middleware' => ['permission:category-delete'], 'uses' => 'CategoryController@destroy'])->name('admin.category.delete');
Route::GET('category-edit',['middleware' => ['permission:category-edit'], 'uses' => 'CategoryController@edit'])->name('admin.category.edit');
Route::POST('add-sub-category-store',['middleware' => ['permission:add-sub-category-store'], 'uses' => 'CategoryController@subCategoryStore'])->name('add.admin.category.store');
Route::GET('add-sub-category-create',['middleware' => ['permission:add-sub-category-create'], 'uses' => 'CategoryController@createSub'])->name('add.admin.category.create');
Route::GET('add-admin-category-view', ['middleware' => ['permission:add-admin-category-view'], 'uses' => 'CategoryController@viewSubCategory'])->name('add.admin.category.view');
Route::GET('add-sub-category-edit', ['middleware' => ['permission:add-sub-category-edit'], 'uses' => 'CategoryController@subCategoryEdit'])->name('add.admin.category.edit');


    /**
 * CategoryController route
 **/
Route::GET('sub-category-listing','SubCategoryController@index')->name('admin.subcategory.listing');
Route::GET('sub-category-create','SubCategoryController@create')->name('admin.subcategory.create');
Route::POST('sub-category-save','SubCategoryController@save')->name('admin.subcategory.save');
Route::GET('sub-category-delete','SubCategoryController@destroy')->name('admin.subcategory.delete');
Route::GET('sub-category-edit','SubCategoryController@edit')->name('admin.subcategory.edit');


/**
 * CategoryController route
 **/
Route::GET('child-category-listing','ChildCategoryController@index')->name('admin.childcategory.listing');
Route::GET('child-category-create','ChildCategoryController@create')->name('admin.childcategory.create');
Route::POST('child-category-save','ChildCategoryController@save')->name('admin.childcategory.save');
Route::GET('child-category-delete','ChildCategoryController@destroy')->name('admin.childcategory.delete');
Route::GET('child-category-edit','ChildCategoryController@edit')->name('admin.childcategory.edit');

/**
 * TaxController route
 **/
Route::GET('tax-listing',['middleware' => ['permission:tax-listing'], 'uses' => 'TaxController@index'])->name('admin.tax.listing');
Route::GET('tax-create', ['middleware' => ['permission:tax-create'], 'uses' => 'TaxController@create'])->name('admin.tax.create');
Route::POST('tax-save', ['middleware' => ['permission:tax-save'], 'uses' => 'TaxController@store'])->name('admin.tax.save');
Route::GET('tax-delete', ['middleware' => ['permission:tax-delete'], 'uses' => 'TaxController@destroy'])->name('admin.tax.delete');
Route::GET('tax-edit',['middleware' => ['permission:tax-edit'], 'uses' => 'TaxController@edit'])->name('admin.tax.edit');


/**
 * ProfileController route
 **/

Route::GET('profile-edit','ProfileController@edit')->name('admin.profile.edit');
Route::get('reset-password', 'Auth\ResetPasswordController@resetPassword')->name('create.reset.password');
Route::post('reset-password', 'Auth\ResetPasswordController@resetpwd')->name('update.reset.password');

Route::post('update-password', 'Auth\ForgotPasswordController@updatePassword')->name('update.reset.password.confirm');


});





